<#import  "lib/technologyslides.ftl" as s>
\usetheme{${theme}}

\institute{${institution}}
\date{\today}
\version{1.0.0}

\usepackage{multicol}
\usepackage[nochapter]{vhistory}

\include{townplan-style}
\include{technologies}

\title{Technology Radar}
\subtitle{Architecture recommendations on tools, techniques, platforms, languages and frameworks.}
\author{Jurgen Lust}

\begin{document}
\maketitle

\agenda

\section{Introduction}
\begin{frame}{What is the technology radar?}
    The technology radar is a document containing a list of all technologies currently in use within the company, with a \alert{recommendation} about their use.

    Delivery teams can use these recommendations to decide when to \alert{adopt} or when to \alert{phase out} a tool, technique, platform, language or framework.
\end{frame}

\section{Languages \& Frameworks}
<@s.technology_slides technologies=model.languagesAndFrameworks />

\section{Platforms}
<@s.technology_slides technologies=model.platforms />

\section{Tools}
<@s.technology_slides technologies=model.tools />

\section{Techniques}
<@s.technology_slides technologies=model.techniques />

\end{document}