\definecolor{cfourcontainerbg}{RGB}{67,141,213}
\definecolor{cfoursystembg}{RGB}{153,153,153}

\colorlet{capability0Background}{blue}
\colorlet{capability1Background}{brown}
\colorlet{capability2Background}{cyan}
\colorlet{capability3Background}{darkgray}
\colorlet{capability4Background}{gray}
\colorlet{capability5Background}{green}
\colorlet{capability6Background}{lime}
\colorlet{capability7Background}{magenta}
\colorlet{capability8Background}{olive}
\colorlet{capability9Background}{teal}
\colorlet{capability0Foreground}{white}
\colorlet{capability1Foreground}{white}
\colorlet{capability2Foreground}{black}
\colorlet{capability3Foreground}{black}
\colorlet{capability4Foreground}{white}
\colorlet{capability5Foreground}{white}
\colorlet{capability6Foreground}{black}
\colorlet{capability7Foreground}{black}
\colorlet{capability8Foreground}{white}
\colorlet{capability9Foreground}{white}
\colorlet{capabilityLine}{black}
\colorlet{selectedLine}{red}
\colorlet{buildingBlockBackground}{magenta}
\colorlet{buildingBlockForeground}{white}
\colorlet{buildingBlockLine}{black}
\colorlet{containerBackground}{cfourcontainerbg}
\colorlet{systemBackground}{cfoursystembg}
\colorlet{containerForeground}{white}
\colorlet{systemForeground}{white}
\colorlet{relationForeground}{black}
\colorlet{squadBackground}{yellow!20}
\colorlet{squadForeground}{black}
\colorlet{squadBorder}{yellow!80}
\colorlet{person1}{blue}
\colorlet{person2}{yellow}
\colorlet{person3}{red}
\colorlet{person4}{green}
\colorlet{person5}{cyan}
\colorlet{workPackageBackground}{pink}
\colorlet{workPackageForeground}{violet}
\colorlet{workPackageLine}{violet}
\colorlet{stakeholderMapQuadrantBackground}{lightgray!20}

\tikzset{%
    capabilityTitle/.style={draw=none, font=\tiny, inner xsep=1mm, inner ysep=0.5mm,, outer sep=0mm, text centered},
    capability/.style={rectangle, draw=capabilityLine, text centered, rounded corners, font=\tiny, minimum height=1cm, text width=2cm, inner sep=1mm, outer sep=1mm},
    capabilityFiller/.style={rectangle, draw=none, fill=none, text centered, rounded corners, font=\tiny, minimum height=1cm, text width=2cm, inner sep=1mm, outer sep=1mm},
    capabilityBox/.style={inner sep=0mm},
    capability0/.style={capability, fill=capability0Background, text=capability0Foreground},
    capability1/.style={capability, fill=capability1Background, text=capability1Foreground},
    capability2/.style={capability, fill=capability2Background, text=capability2Foreground},
    capability3/.style={capability, fill=capability3Background, text=capability3Foreground},
    capability4/.style={capability, fill=capability4Background, text=capability4Foreground},
    capability5/.style={capability, fill=capability5Background, text=capability5Foreground},
    capability6/.style={capability, fill=capability6Background, text=capability6Foreground},
    capability7/.style={capability, fill=capability7Background, text=capability7Foreground},
    capability8/.style={capability, fill=capability8Background, text=capability8Foreground},
    capability9/.style={capability, fill=capability9Background, text=capability9Foreground},
    capabilityTitle0/.style={capabilityTitle, text=capability0Foreground},
    capabilityTitle1/.style={capabilityTitle, text=capability1Foreground},
    capabilityTitle2/.style={capabilityTitle, text=capability2Foreground},
    capabilityTitle3/.style={capabilityTitle, text=capability3Foreground},
    capabilityTitle4/.style={capabilityTitle, text=capability4Foreground},
    capabilityTitle5/.style={capabilityTitle, text=capability5Foreground},
    capabilityTitle6/.style={capabilityTitle, text=capability6Foreground},
    capabilityTitle7/.style={capabilityTitle, text=capability7Foreground},
    capabilityTitle8/.style={capabilityTitle, text=capability8Foreground},
    capabilityTitle9/.style={capabilityTitle, text=capability9Foreground},
    flow/.style={draw=relationForeground, dashed, font=\tiny, text=relationForeground},
    composition/.style={draw=relationForeground},
    association/.style={draw=relationForeground},
    realization/.style={draw=relationForeground, dotted},
    relationLabel/.style={font=\tiny, text=relationForeground, text centered},
    buildingBlock/.style={rectangle, draw=buildingBlockLine, fill=buildingBlockBackground, text=buildingBlockForeground, text centered, rounded corners, text width=2cm, font=\tiny, minimum height=1cm, inner sep=1mm, outer sep=1mm},
    buildingBlockFiller/.style={rectangle, draw=none, fill=none, text centered, rounded corners, font=\tiny, minimum height=1cm, text width=2cm, inner sep=1mm, outer sep=1mm},
    selected/.style={line width=2mm, draw=selectedLine},
    container/.style={rectangle, draw=none, text centered, rounded corners, font=\tiny, minimum height=1cm, inner sep=2mm, outer sep=0mm, fill=containerBackground, text=containerForeground},
    system/.style={rectangle, draw=none, text centered, rounded corners, font=\tiny, minimum height=1cm, inner sep=2mm, outer sep=0mm, fill=systemBackground, text=systemForeground},
    systemContext/.style={rectangle, draw=black, dashed, rounded corners, font=\tiny, minimum height=1cm, inner ysep=5mm, inner xsep=2mm, outer sep=0mm, fill=none, text=black},
    systemContextLabel/.style={font=\tiny \bfseries, fill=none, text=black, text height=5mm},
    squad_name_node/.style={circle, thick, minimum size=2cm, text width=1.7cm, text centered, draw=squadBorder, fill=squadBackground, font=\tiny, text=squadForeground},
    personBase/.style={minimum size=1cm, font=\tiny, text=squadForeground},
    person1/.style={personBase, person, shirt=person1},
    person2/.style={personBase, bob, shirt=person2},
    person3/.style={personBase, charlie, shirt=person3},
    person4/.style={personBase, person, shirt=person5},
    person5/.style={personBase, charlie, shirt=person4},
    person6/.style={personBase, criminal, shirt=person1},
    person7/.style={personBase, pilot, shirt=person2},
    person8/.style={personBase, maninblack, shirt=person3},
    person9/.style={personBase, bob, shirt=person5},
    person10/.style={personBase, person, shirt=person1},
    person11/.style={personBase, charlie, shirt=person3},
    person12/.style={personBase, criminal, shirt=person4},
    person13/.style={personBase, sailor, shirt=person2},
    person14/.style={personBase, conductor, shirt=person5},
    person15/.style={personBase, builder, shirt=person4},
    person16/.style={personBase, cowboy, shirt=person3},
    person17/.style={personBase, jester, shirt=person2},
    person18/.style={personBase, duck, shirt=person1},
    person19/.style={personBase, bob, shirt=person4},
    person20/.style={personBase, dave, shirt=person2},
    person21/.style={personBase, criminal, shirt=person4},
    workPackage/.style={rectangle, draw=workPackageLine, fill=workPackageBackground, text=workPackageForeground, text centered, rounded corners, font=\tiny, minimum height=1cm, inner sep=2mm, outer sep=0mm},
    stakeholderMapQuadrant/.style={rectangle, draw=none, inner sep=2mm, outer sep=0mm, fill=stakeholderMapQuadrantBackground},
    stakeholderMapPlusMinus/.style={circle, fill=black, text=white, inner sep=1pt, outer sep=5pt, minimum width=5mm, text centered},
    stakeholderMapPerson/.style={font=\tiny,minimum size=1em, text centered}
}

