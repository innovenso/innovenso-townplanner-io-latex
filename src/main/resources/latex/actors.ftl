<#import "lib/texify.ftl" as tex>

<#list model.people as actor>
    \newcommand{\Person${actor.camelCasedKey}Title}{<@tex.texify text=actor.title />}

    \newcommand{\Person${actor.camelCasedKey}Description}{<@tex.texify text=actor.description />}

    \newcommand{\Person${actor.camelCasedKey}Key}{${actor.key}}

    \newcommand{\Person${actor.camelCasedKey}TitleKey}{${actor.key}_title}
</#list>

<#list model.squads as actor>
    \newcommand{\Squad${actor.camelCasedKey}Title}{<@tex.texify text=actor.title />}

    \newcommand{\Squad${actor.camelCasedKey}Description}{<@tex.texify text=actor.description />}

    \newcommand{\Squad${actor.camelCasedKey}Key}{${actor.key}}

    \newcommand{\Squad${actor.camelCasedKey}TitleKey}{${actor.key}_title}

    \newcommand{\Squad${actor.camelCasedKey}MindMap}{\includegraphics{${actor.key}.eps}}

    \newcommand{\Squad${actor.camelCasedKey}MindMapDrawing}{\drawing[\Squad${actor.camelCasedKey}Title]{${actor.key}.eps}}
</#list>