<#import "lib/texify.ftl" as tex>

<#list model.systems as system>
    \newcommand{\System${system.camelCasedKey}Title}{<@tex.texify text=system.title />}

    \newcommand{\System${system.camelCasedKey}Description}{<@tex.texify text=system.description />}

    \newcommand{\System${system.camelCasedKey}Key}{${system.key}}

    \newcommand{\System${system.camelCasedKey}TitleKey}{${system.key}_title}

    \newcommand{\System${system.camelCasedKey}ContextDiagramAsIs}{\includegraphics{${system.key}-as-is-today.eps}}

    \newcommand{\System${system.camelCasedKey}ContextDiagramDrawing}{\drawing[\System${system.camelCasedKey}Title]{${system.key}.eps}}

    \newcommand{\System${system.camelCasedKey}AwsDiagram}{\includegraphics{${system.key}-aws-deployment.eps}}

    \newcommand{\System${system.camelCasedKey}AwsDiagramDrawing}{\drawing[\System${system.camelCasedKey}Title]{${system.key}-aws-deployment.eps}}

    \newcommand{\System${system.camelCasedKey}IntegrationDiagram}{\includegraphics{${system.key}-integrations.eps}}

    \newcommand{\System${system.camelCasedKey}IntegrationDiagramDrawing}{\drawing[\System${system.camelCasedKey}Title]{${system.key}-integrations.eps}}
</#list>
