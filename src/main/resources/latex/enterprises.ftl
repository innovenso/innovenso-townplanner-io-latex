<#import "lib/texify.ftl" as tex>

<#list model.enterprises as enterprise>
    \newcommand{\Enterprise${enterprise.camelCasedKey}Title}{<@tex.texify text=enterprise.title />}

    \newcommand{\Enterprise${enterprise.camelCasedKey}Key}{${enterprise.key}}

    \newcommand{\Enterprise${enterprise.camelCasedKey}TitleKey}{${enterprise.key}_title}

    \newcommand{\Enterprise${enterprise.camelCasedKey}Description}{<@tex.texify text=enterprise.description />}

    \newcommand{\Enterprise${enterprise.camelCasedKey}MindMap}{\includegraphics{${enterprise.key}.eps}}

    \newcommand{\Enterprise${enterprise.camelCasedKey}TownPlan}{\includegraphics{${enterprise.key}-townplan.eps}}

    \newcommand{\Enterprise${enterprise.camelCasedKey}MindMapDrawing}{\drawing[\Enterprise${enterprise.camelCasedKey}Title]{${enterprise.key}.eps}}

    \newcommand{\Enterprise${enterprise.camelCasedKey}TownPlanDrawing}{\drawing[\Enterprise${enterprise.camelCasedKey}Title]{${enterprise.key}-townplan.eps}}
</#list>
