<#import "lib/relationship.ftl" as r>

% System Container Diagram for ${element.title}

\begin{tikzpicture}[node distance=1cm and 2cm]

<#-- the central system and its containers -->
<#if element.hasContainers()>
    <#list element.containers as container>
        \Container${container.camelCasedKey} {2cm} %\Container${container.camelCasedKey}Key
    </#list>

    \SystemContext${element.centralSystem.camelCasedKey} {<#list element.containers as container> (\Container${container.camelCasedKey}Key)</#list>}
<#else>
    \System${element.centralSystem.camelCasedKey} {2cm} %\System${element.centralSystem.camelCasedKey}Key
</#if>

<#-- render all the systems that interact with the system itself or any of the containers inside this system -->
<#list element.systems as system>
    \System${system.camelCasedKey} {2cm} %\System${system.camelCasedKey}Key
</#list>

<#-- render all the actors that interact with the system itself or any of the containers inside this system -->
<#list element.actors as actor>
    \Person${actor.camelCasedKey} %\Person${actor.camelCasedKey}Key
</#list>

<#-- render all relationships -->
<#list element.relationships as relationship>
    <@r.element_relationship relationship=relationship title=relationship.title />
</#list>

\end{tikzpicture}


