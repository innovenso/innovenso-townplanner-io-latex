<#import "lib/texify.ftl" as tex>

<#list model.milestones as milestone>
    \newcommand{\Milestone${milestone.camelCasedKey}Title}{<@tex.texify text=milestone.title />}

    \newcommand{\Milestone${milestone.camelCasedKey}Description}{<@tex.texify text=milestone.description />}

    \newcommand{\Milestone${milestone.camelCasedKey}Key}{${milestone.key}}

    \newcommand{\Milestone${milestone.camelCasedKey}TitleKey}{${milestone.key}_title}

    \newcommand{\Milestone${milestone.camelCasedKey}CapabilityImpactDiagram}{\includegraphics{${milestone.key}-capability-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}CapabilityImpactDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - Business Capability Impact]{${milestone.key}-capability-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}BuildingBlockImpactDiagram}{\includegraphics{${milestone.key}-building-block-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}BuildingBlockImpactDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - Building Block Impact]{${milestone.key}-building-block-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}EntityImpactDiagram}{\includegraphics{${milestone.key}-data-entity-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}EntityImpactDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - Information Model Impact]{${milestone.key}-data-entity-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}SystemImpactDiagram}{\includegraphics{${milestone.key}-system-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}SystemImpactDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - System Impact]{${milestone.key}-system-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}IntegrationImpactDiagram}{\includegraphics{${milestone.key}-system-integration-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}IntegrationImpactDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - Integration Impact]{${milestone.key}-system-integration-impact.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}IntegrationMapDiagram}{\includegraphics{${milestone.key}-system-integration-map.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}IntegrationMapDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - Integrations]{${milestone.key}-system-integration-map.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}AsIsDiagram}{\includegraphics{${milestone.key}-as-is.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}AsIsDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - As Is]{${milestone.key}-as-is.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}ToBeDiagram}{\includegraphics{${milestone.key}-to-be.eps}}

    \newcommand{\Milestone${milestone.camelCasedKey}ToBeDiagramDrawing}{\drawing[\Milestone${milestone.camelCasedKey}Title - To Be]{${milestone.key}-to-be.eps}}

</#list>
