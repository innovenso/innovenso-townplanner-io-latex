<#import "lib/criticality.ftl" as c>
<#import "lib/texify.ftl" as tex>

<#list model.integrations as integration>
    \newcommand{\Integration${integration.camelCasedKey}Title}{<@tex.texify text=integration.title />}

    \newcommand{\Integration${integration.camelCasedKey}Description}{<@tex.texify text=integration.description />}

    \newcommand{\Integration${integration.camelCasedKey}Technologies}{<@tex.texify text=integration.technologyDescription />}

    \newcommand{\Integration${integration.camelCasedKey}Key}{${integration.key}}

    \newcommand{\Integration${integration.camelCasedKey}Criticality}{<@c.criticality criticality=integration.criticalityLabel />}

    \newcommand{\Integration${integration.camelCasedKey}CriticalityDescription}{<@tex.texify text=integration.criticalityDescription.orElse('') />}

    \newcommand{\Integration${integration.camelCasedKey}Volume}{<@tex.texify text=integration.volume.orElse('') />}

    \newcommand{\Integration${integration.camelCasedKey}Frequency}{<@tex.texify text=integration.frequency.orElse('') />}

    \newcommand{\Integration${integration.camelCasedKey}Resilience}{<@tex.texify text=integration.resilience.orElse('') />}

    \newcommand{\Integration${integration.camelCasedKey}TitleKey}{${integration.key}_title}

    \newcommand{\Integration${integration.camelCasedKey}Diagram}{\includegraphics{${integration.key}.eps}}

    \newcommand{\Integration${integration.camelCasedKey}DiagramDrawing}{\drawing[\Integration${integration.camelCasedKey}Title]{${integration.key}.eps}}

    \newcommand{\Integration${integration.camelCasedKey}SimpleDiagram}{\includegraphics{${integration.key}-simple.eps}}

    \newcommand{\Integration${integration.camelCasedKey}SimpleDiagramDrawing}{\drawing[\Integration${integration.camelCasedKey}Title]{${integration.key}-simple.eps}}

    \newcommand{\Integration${integration.camelCasedKey}FlowDiagram}{\includegraphics{${integration.key}-flows.eps}}

    \newcommand{\Integration${integration.camelCasedKey}FlowDiagramDrawing}{\drawing[\Integration${integration.camelCasedKey}Title]{${integration.key}-flows.eps}}

    \newcommand{\Integration${integration.camelCasedKey}SequenceDiagram}{\includegraphics{${integration.key}-sequence.eps}}

    \newcommand{\Integration${integration.camelCasedKey}SequenceDiagramDrawing}{\drawing[\Integration${integration.camelCasedKey}Title]{${integration.key}-sequence.eps}}
</#list>
