% Architecture Building Block map for ${element.title}

<#list element.levelZeroBusinessCapabilities as cap>
    % ${cap.title} - START
    \Capability${cap.camelCasedKey}BoxTitle %\Capability${cap.camelCasedKey}TitleKey

    <#list element.getBuildingBlocks(cap.key) as bb>
        \CapabilityBuildingBlock${cap.camelCasedKey}${bb.camelCasedKey} %\CapabilityBuildingBlock${cap.camelCasedKey}${bb.camelCasedKey}Key
    </#list>

    \Capability${cap.camelCasedKey}Box{(\Capability${cap.camelCasedKey}TitleKey)<#list element.getBuildingBlocks(cap.key) as bb> (\CapabilityBuildingBlock${cap.camelCasedKey}${bb.camelCasedKey}Key)</#list>}

    % ${cap.title} - END

</#list>