<#import "lib/texify.ftl" as tex>

<#list model.views as view>
    \newcommand{\Flow${view.camelCasedKey}Title}{<@tex.texify text=view.title />}

    \newcommand{\Flow${view.camelCasedKey}Description}{<@tex.texify text=view.description />}

    \newcommand{\Flow${view.camelCasedKey}Key}{${view.key}}

    \newcommand{\Flow${view.camelCasedKey}TitleKey}{${view.key}_title}

    \newcommand{\Flow${view.camelCasedKey}FlowDiagram}{\includegraphics{${view.key}.eps}}

    \newcommand{\Flow${view.camelCasedKey}FlowDiagramDrawing}{\drawing[\Flow${view.camelCasedKey}Title]{${view.key}.eps}}

    \newcommand{\Flow${view.camelCasedKey}SequenceDiagram}{\includegraphics{${view.key}-sequence.eps}}

    \newcommand{\Flow${view.camelCasedKey}SequenceDiagramDrawing}{\drawing[\Flow${view.camelCasedKey}Title]{${view.key}-sequence.eps}}
</#list>
