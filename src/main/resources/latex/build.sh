#!/usr/bin/env bash

DECK_NAME=$1
OUTPUT_FILENAME="${DECK_NAME}"
OUTPUT_VERSION=0.0.1
ROOT_DIR=`pwd`
DECK_DIR="${ROOT_DIR}/src/decks/${DECK_NAME}"
BUILD_DIR="${ROOT_DIR}/build/"
OUTPUT_DIR="${ROOT_DIR}/output/"
TOWNPLAN_DIR="${ROOT_DIR}/src/townplan"
THEME_DIR="${ROOT_DIR}/theme"
IMAGES_DIR="${ROOT_DIR}/images"
LIB_DIR="${ROOT_DIR}/lib"
PRESENTATION_INPUT_FILE=presentation
HANDOUT_INPUT_FILE=handouts
HANDOUT_WITH_NOTES_INPUT_FILE=handouts_with_notes
IMAGE_TESTER_INPUT_FILE=image_tester

function clean {
	rm -rf "${BUILD_DIR}"
	rm -rf "${OUTPUT_DIR}"
}


function buildSourceFile {
#	determineVersion
	local INPUT_FILE=$1
	local OUTPUT_SUFFIX=$2
	rm -rf "${BUILD_DIR}"
	mkdir -p "${BUILD_DIR}"
	cp -R "${DECK_DIR}"/* "${BUILD_DIR}"
	mkdir -p "${BUILD_DIR}/townplan"
	cp -R "${TOWNPLAN_DIR}"/* "${BUILD_DIR}"
	cp -R "${IMAGES_DIR}"/* "${BUILD_DIR}"
 	cp -R "${THEME_DIR}"/* "${BUILD_DIR}"
 	cp -R "${LIB_DIR}"/* "${BUILD_DIR}"
	cd "${BUILD_DIR}"
	latexmk -pdf "${INPUT_FILE}.tex"
	cd "${ROOT_DIR}"
	mkdir -p "${OUTPUT_DIR}"
	cp "$BUILD_DIR/${INPUT_FILE}.pdf" "${OUTPUT_DIR}/${OUTPUT_FILENAME}-${OUTPUT_SUFFIX}-${OUTPUT_VERSION}.pdf"
	cp "$BUILD_DIR/${INPUT_FILE}.pdf" "${OUTPUT_DIR}/${OUTPUT_FILENAME}-${OUTPUT_SUFFIX}-latest.pdf"
	rm -rf "${BUILD_DIR}"
}

function tikz {
    buildSourceFile ${IMAGE_TESTER_INPUT_FILE} "image"
}

function handout {
    buildSourceFile ${HANDOUT_INPUT_FILE} "handout"
}

function presentation {
    buildSourceFile ${PRESENTATION_INPUT_FILE} "presentation"
}

function notes {
    buildSourceFile ${HANDOUT_WITH_NOTES_INPUT_FILE} "notes"
}

function build {
    handout
    presentation
    notes
}

function determineVersion {
	LATEST_REVISION=`git rev-list --tags --skip=0 --max-count=1`
	CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`

	if [ -z "${LATEST_REVISION}" ]
	then
		TAG_NAME="0.0.0"
	else
		TAG_NAME="`git describe --abbrev=0 --tags $LATEST_REVISION`"
    	DEPLOY_DESCRIPTION="`git tag -l --format='%(contents)' ${TAG_NAME}`"
	fi

	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))"

	if [ $CURRENT_BRANCH == "master" ]
	then
	    THIS_VERSION=${TAG_NAME}
	else
    	THIS_VERSION="${TAG_NAME%.*}.$((${TAG_NAME##*.}+1))-SNAPSHOT"
	fi
	OUTPUT_VERSION="${THIS_VERSION}"
	echo "version [${OUTPUT_VERSION} ($CURRENT_BRANCH)]"
}

	
case "$2" in
        clean)
            clean
	    	;;
		version)
			determineVersion
			;;
		tikz)
		    tikz
		    ;;
		handout)
		    handout
		    ;;
		presentation)
		    presentation
		    ;;
		notes)
		    notes
		    ;;
        *)
            build
            ;;
esac
