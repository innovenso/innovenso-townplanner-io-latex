% Business capability map for ${element.title}

<#list element.levelZeroBusinessCapabilities as cap>
    % ${cap.title} - START
    \Capability${cap.camelCasedKey}BoxTitle %\Capability${cap.camelCasedKey}TitleKey

    <#list element.getLevelOneBusinessCapabilities(cap.key) as cap1>
        \Capability${cap1.camelCasedKey} %\Capability${cap1.camelCasedKey}Key
    </#list>
    \Capability${cap.camelCasedKey}Box{(\Capability${cap.camelCasedKey}TitleKey)<#list element.getLevelOneBusinessCapabilities(cap.key) as cap1> (\Capability${cap1.camelCasedKey}Key)</#list>}

    % ${cap.title} - END

</#list>