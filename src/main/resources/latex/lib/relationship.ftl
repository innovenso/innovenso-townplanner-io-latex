<#macro element_relationship relationship title>

    <#switch relationship.relationshipType.label>
        <#case "realization">
            \path [-{Triangle[open]},realization] (${relationship.source.key}) -- node[above,sloped,relationLabel, text width=2cm] {realizes} (${relationship.target.key});
            <#break>
        <#case "composition">
            \path [-{Diamond},composition] (${relationship.source.key}) -- (${relationship.target.key});
            <#break>
        <#case "member">
            \path [-,association] (${relationship.source.key}) -- node[above,sloped,relationLabel, text width=2cm] {${title!relationship.title?replace("&","\\&")}} (${relationship.target.key});
            <#break>
        <#default>
            <#if relationship.bidirectional>
                \path [{Triangle}-{Triangle},flow] (${relationship.source.key}) -- node[above,sloped,relationLabel, text width=2cm] {${title!relationship.title?replace("&","\\&")}} (${relationship.target.key});
            <#else>
                \path [-{Triangle},flow] (${relationship.source.key}) -- node[above,sloped,relationLabel, text width=2cm] {${title!relationship.title?replace("&","\\&")}} (${relationship.target.key});
            </#if>
    </#switch>
</#macro>