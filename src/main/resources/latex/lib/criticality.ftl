<#macro criticality criticality>

    <#switch criticality>
        <#case "Very High">
            \textcolor{red}{\faThermometerFull}
            <#break>
        <#case "High">
            \textcolor{orange}{\faThermometerThreeQuarters}
            <#break>
        <#case "Medium">
            \textcolor{yellow}{\faThermometerHalf}
            <#break>
        <#case "Very Low">
            \textcolor{green}{\faThermometerEmpty}
            <#break>
        <#case "Low">
            \textcolor{blue}{\faThermometerQuarter}
            <#break>
        <#default>
            \textcolor{gray}{\faQuestion}
    </#switch>
</#macro>