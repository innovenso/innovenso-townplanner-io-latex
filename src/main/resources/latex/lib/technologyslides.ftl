<#macro technology_slides technologies>
    <#list technologies as technology>
    \begin{frame}{ \Technology${technology.camelCasedKey}Type~-~\Technology${technology.camelCasedKey}Title }
        \begin{tikzpicture}
        \draw[] (1cm,0) arc[start angle=0, end angle=90,radius=1cm];
        \draw[] (2cm,0) arc[start angle=0, end angle=90,radius=2cm];
        \draw[] (3cm,0) arc[start angle=0, end angle=90,radius=3cm];
        \draw[] (0,4cm) arc[start angle=90, end angle=0,radius=4cm];

        \draw (0,0) -- (0,4cm);

        \node[rotate=90, text=lightgray, font=\footnotesize, text centered] at (-2mm, 2cm) (){ \Technology${technology.camelCasedKey}Type };

        \draw (0,0) -- (1cm,0) node [font=\tiny, anchor=north, midway, black, text height=2mm] { adopt };
        \draw (1cm,0) -- (2cm,0) node [font=\tiny, anchor=north, midway, black, text height=2mm] { trial };
        \draw (2cm,0) -- (3cm,0) node [font=\tiny, anchor=north, midway, black, text height=2mm] { assess };
        \draw (3cm,0) -- (4cm,0) node [font=\tiny, anchor=north, midway, black, text height=2mm] { hold };

        <#switch technology.recommendation.label>
            <#case "Adopt">
                \node[font=\small] at (45:0.5cm)  { \faMapMarker* };
                <#break>
            <#case "Trial">
                \node[font=\small] at (45:1.5cm)  { \faMapMarker* };
                <#break>
            <#case "Asses">
                \node[font=\small] at (45:2.5cm)  { \faMapMarker* };
                <#break>
            <#case "Hold">
                \node[font=\small] at (45:3.5cm)  { \faMapMarker* };
                <#break>
        </#switch>

        \node[font=\bfseries, text width=8cm, align=left, anchor=south west] at (5cm,4.5cm) {\Technology${technology.camelCasedKey}Title};
        \node[font=\scriptsize, text width=8cm, align=left, anchor=north west] at (5cm, 4.2cm) {\Technology${technology.camelCasedKey}Description};
        \end{tikzpicture}
    \end{frame}

    </#list>
</#macro>