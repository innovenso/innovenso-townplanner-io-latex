<#import "texify.ftl" as tex>

<#macro nemawashi model>

    \begin{frame}{Nemawashi~\begin{CJK}{UTF8}{goth}根回し\end{CJK}}
    The following people were involved in the architecture of this milestone:

    \begin{description}[align=left,labelwidth=4cm]
        <#list model.nemawashiDone as nema>
            \item[<@tex.texify text=nema.type />] <@tex.texify text=nema.title /> \taskComplete
        </#list>
        <#list model.nemawashiTodo as nema>
            \item[<@tex.texify text=nema.type />] <@tex.texify text=nema.title /> \taskTodo
        </#list>
    \end{description}
    \end{frame}


</#macro>