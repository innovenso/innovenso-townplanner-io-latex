<#import "texify.ftl" as tex>

<#macro securityconcerns concerns>
    \coordinate(concernstart) at (0cm, 6cm);

    <#assign concern_coordinate="concernstart">
    <#list concerns as concern>
        \node[anchor=north west, description_box, font=\scriptsize\bfseries] (${concern.key}_title) at (${concern_coordinate}) {<@tex.texify text=concern.title />};
        \node[below=of ${concern.key}_title.south west, anchor=north west, description_box] (${concern.key}_description) {<@tex.texify text=concern.description />};
        \coordinate[below=of ${concern.key}_description.south west](${concern.key}_bottom);
        <#assign concern_coordinate="${concern.key}_bottom">
    </#list>
</#macro>