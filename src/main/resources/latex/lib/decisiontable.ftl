<#macro decision_table conceptModel>
\begin{frame}[t]{Architecture Decision Record}
    \begin{tiny}
        \begin{table}
            \begin{tabular}{  @{}p{5cm}p{1cm}p{7cm}@{}  }
                \toprule
                    \faBrain & \faGavel & \faFileSignature \\
                \midrule
                    <#list conceptModel.decisions as decision>
                                    \hyperlink{\Decision${decision.camelCasedKey}Key}{\Decision${decision.camelCasedKey}Title} & \Decision${decision.camelCasedKey}Status & \Decision${decision.camelCasedKey}Outcome \\
                        <#if decision?counter % 5 == 0 && decision?counter != 0 && decision?has_next>
                                            \bottomrule
                                        \end{tabular}
                                    \end{table}
                                \end{tiny}
                            \end{frame}

                            \begin{frame}[t]{Architecture Decision Record}
                                \begin{tiny}
                                    \begin{table}
                                        \begin{tabular}{  @{}p{5cm}p{1cm}p{7cm}@{}  }
                                            \toprule
                                                \faBrain & \faGavel & \faFileSignature \\
                                            \midrule
                        </#if>
                    </#list>
                \bottomrule
            \end{tabular}
        \end{table}
    \end{tiny}
\end{frame}
</#macro>

