<#import "texify.ftl" as tex>

<#macro swot conceptModel>
<#if conceptModel.threats?has_content>
    \begin{frame}{Risks}
    <#list conceptModel.threats as threat>
        \alert{<@tex.texify text=threat.description />}
    </#list>
    \end{frame}
</#if>

</#macro>