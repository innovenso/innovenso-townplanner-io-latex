<#import "texify.ftl" as tex>

<#macro functionalRequirements conceptModel>
    <#if conceptModel.functionalRequirements?has_content>
        \begin{frame}{Functional Requirements}
        \begin{description}[labelwidth=4cm]
        <#list conceptModel.functionalRequirements as req>
            \item[<@tex.texify text=req.title />] <@tex.texify text=req.description />
        </#list>
        \end{description}
        \end{frame}
    </#if>
</#macro>