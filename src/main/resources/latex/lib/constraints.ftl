<#import "texify.ftl" as tex>

<#macro constraints constraints>

    \begin{frame}{Constraints}
    <#list constraints as constraint>
        \alert{<@tex.texify text=constraint.title />}

        <@tex.texify text=constraint.description />

    </#list>

    \end{frame}


</#macro>