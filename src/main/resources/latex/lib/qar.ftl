<#import "texify.ftl" as tex>

<#macro qar conceptModel>
    <#if conceptModel.qars?has_content>
        <#list conceptModel.qars as req>
        \begin{frame}{QAR Scenario - <@tex.texify text=req.title />}
        \begin{description}[labelwidth=4cm]
            <#if req.sourceOfStimulus?has_content>
                \item[Source of stimulus] <@tex.texify text=req.sourceOfStimulus />
            </#if>
            <#if req.stimulus?has_content>
                \item[Stimulus] <@tex.texify text=req.stimulus />
            </#if>
            <#if req.environment?has_content>
                \item[Environment] <@tex.texify text=req.environment />
            </#if>
            <#if req.response?has_content>
                \item[Response] <@tex.texify text=req.response />
            </#if>
            <#if req.responseMeasure?has_content>
                \item[Response Measure] <@tex.texify text=req.responseMeasure />
            </#if>
        \end{description}
        \end{frame}
        </#list>
    </#if>
</#macro>