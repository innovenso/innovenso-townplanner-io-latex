<#macro integration_slides integrations>
    <#list integrations as integration>
    \subsubsection*{ \Integration${integration.camelCasedKey}Title }
    \begin{frame}[t]{ \Integration${integration.camelCasedKey}Title }
        \begin{tikzpicture}[node distance=1mm and 1mm]
            \node[inner sep=0pt, text width=8cm] (integration_diagram) at (0,0) {\includegraphics[width=\textwidth]{${integration.key}-simple.eps}};
            \node[text width=8cm, below=5mm of integration_diagram, font=\scriptsize] (integration_description) {\hypertarget{\Integration${integration.camelCasedKey}Key}\Integration${integration.camelCasedKey}Description};

            \node[font=\tiny, right=10mm of integration_diagram.north east, anchor=north west, text width=5mm, align=right] (technologies_icon) {\faNetworkWired};
            \node[font=\tiny, right=of technologies_icon.north east, anchor=north west, text width=3.8cm, align=left] (technologies) {\Integration${integration.camelCasedKey}Technologies};

            \node[font=\tiny, below=5mm of technologies, text width=3.8cm, align=left] (criticality) {\Integration${integration.camelCasedKey}Criticality~\Integration${integration.camelCasedKey}CriticalityDescription};
            \node[font=\tiny, left=of criticality.north west, anchor=north east, text width=5mm, align=right] (criticality_icon) {\faExclamationCircle};

            \node[font=\tiny, below=of criticality, text width=3.8cm, align=left] (volume) {\Integration${integration.camelCasedKey}Volume};
            \node[font=\tiny, left=of volume.north west, anchor=north east, text width=5mm, align=right] (volume_icon) {\faWifi};

            \node[font=\tiny, below=of volume, text width=3.8cm, align=left] (frequency) {\Integration${integration.camelCasedKey}Frequency};
            \node[font=\tiny, left=of frequency.north west, anchor=north east, text width=5mm, align=right] (frequency_icon) {\faHeartbeat};

            \node[font=\tiny, below=of frequency, text width=3.8cm, align=left] (resilience) {\Integration${integration.camelCasedKey}Resilience};
            \node[font=\tiny, left=of resilience.north west, anchor=north east, text width=5mm, align=right] (resilience_icon) {\faBomb};
        \end{tikzpicture}
    \end{frame}

        <#if integration.steps?size != 0>
            \begin{frame}{ \Integration${integration.camelCasedKey}Title - Flow}
            \Integration${integration.camelCasedKey}FlowDiagramDrawing
            \end{frame}

            \begin{frame}{ \Integration${integration.camelCasedKey}Title - Sequence}
            \Integration${integration.camelCasedKey}SequenceDiagramDrawing
            \end{frame}
        </#if>
    </#list>
</#macro>