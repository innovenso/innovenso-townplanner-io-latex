<#import "texify.ftl" as tex>
<#import "impact.ftl" as ip>

<#macro securityimpacts impacts>
    \node[anchor=north east, impact_box] (low_label) at (11.5cm,6cm) { L };
    \node[right=of low_label, impact_box] (medium_label) { M };
    \node[right=of medium_label, impact_box] (high_label) { H };
    \node[right=of high_label, impact_box] (extreme_label) { EH };

    \coordinate[below=of extreme_label](impactstart);
    <#assign impact_coordinate="impactstart">

    <#list impacts as impact>
        \node (${impact.key}_extreme)[xhigh] at (${impact_coordinate}) { <#if impact.impactLevel.value="EXTREME">\faCheck </#if>};
        \node (${impact.key}_high)[high, left=of ${impact.key}_extreme] { <#if impact.impactLevel.value="HIGH">\faCheck </#if> };
        \node (${impact.key}_medium)[medium, left=of ${impact.key}_high] {<#if impact.impactLevel.value="MEDIUM">\faCheck </#if>};
        \node (${impact.key}_low)[low, left=of ${impact.key}_medium] { <#if impact.impactLevel.value="LOW">\faCheck </#if> };
        \node (${impact.key}_label)[left=of ${impact.key}_low, font=\tiny, anchor=east] { <@ip.impact impact=impact.impactType.label /> };
        \coordinate[below=3mm of ${impact.key}_extreme](${impact.key}_bottom);
        <#assign impact_coordinate="${impact.key}_bottom">
    </#list>
</#macro>