<#macro impact impact>

    <#switch impact>
        <#case "Confidentiality">
            Confidentiality
            <#break>
        <#case "Integrity">
            Integrity
            <#break>
        <#case "Availability">
            Availability
            <#break>
        <#case "Data Loss">
            Data Loss
            <#break>
    </#switch>
</#macro>