<#macro integration_table integrations title>
\begin{frame}[t]{${title}}
    \begin{tiny}
        \begin{table}
            \begin{tabular}{  @{}p{7cm}p{5cm}p{1cm}@{}  }
                \toprule
                    \faNetworkWired & \faCogs & \faExclamationCircle \\
                \midrule
                    <#list integrations as integration>
                                    \hyperlink{\Integration${integration.camelCasedKey}Key}{\Integration${integration.camelCasedKey}Title} & \Integration${integration.camelCasedKey}Technologies & \Integration${integration.camelCasedKey}Criticality \\
                        <#if integration?counter % 15 == 0 && integration?counter != 0 && integration?has_next>
                                            \bottomrule
                                        \end{tabular}
                                    \end{table}
                                \end{tiny}
                            \end{frame}

                            \begin{frame}[t]{${title}}
                                \begin{tiny}
                                    \begin{table}
                                        \begin{tabular}{  @{}p{7cm}p{5cm}p{1cm}@{}  }
                                            \toprule
                                                \faNetworkWired & \faCogs & \faExclamationCircle \\
                                            \midrule
                        </#if>
                    </#list>
                \bottomrule
            \end{tabular}
        \end{table}
    \end{tiny}
\end{frame}
</#macro>

