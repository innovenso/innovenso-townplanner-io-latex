<#import "lib/texify.ftl" as tex>

<#list model.capabilities as cap>
    \newcommand{\Capability${cap.camelCasedKey}Title}{<@tex.texify text=cap.title />}

    \newcommand{\Capability${cap.camelCasedKey}Description}{<@tex.texify text=cap.description />}

    \newcommand{\Capability${cap.camelCasedKey}Key}{${cap.key}}

    \newcommand{\Capability${cap.camelCasedKey}TitleKey}{${cap.key}_title}

    \newcommand{\Capability${cap.camelCasedKey}MindMap}{\includegraphics{${cap.key}.eps}}

    \newcommand{\Capability${cap.camelCasedKey}MindMapDrawing}{\drawing[\Capability${cap.camelCasedKey}Title]{${cap.key}.eps}}

</#list>
