<#import "lib/texify.ftl" as tex>

<#list model.decisions as dec>
    \newcommand{\Decision${dec.camelCasedKey}Title}{<@tex.texify text=dec.title />}

    \newcommand{\Decision${dec.camelCasedKey}Description}{<@tex.texify text=dec.description />}

    \newcommand{\Decision${dec.camelCasedKey}Status}{<@tex.texify text=dec.status.label />}

    \newcommand{\Decision${dec.camelCasedKey}Outcome}{<@tex.texify text=dec.outcome />}

    \newcommand{\Decision${dec.camelCasedKey}Key}{${dec.key}}

    \newcommand{\Decision${dec.camelCasedKey}TitleKey}{${dec.key}_title}

</#list>
