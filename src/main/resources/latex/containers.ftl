<#import "lib/texify.ftl" as tex>

<#list model.containers as container>
    \newcommand{\Container${container.camelCasedKey}Title}{<@tex.texify text=container.title />}

    \newcommand{\Container${container.camelCasedKey}Description}{<@tex.texify text=container.description />}

    \newcommand{\Container${container.camelCasedKey}Technologies}{<@tex.texify text=container.technologyDescription />}

    \newcommand{\Container${container.camelCasedKey}Key}{${container.key}}

    \newcommand{\Container${container.camelCasedKey}TitleKey}{${container.key}_title}

</#list>
