<#import "lib/texify.ftl" as tex>

<#list model.technologies as technology>
    \newcommand{\Technology${technology.camelCasedKey}Title}{<@tex.texify text=technology.title />}

    \newcommand{\Technology${technology.camelCasedKey}Description}{<@tex.texify text=technology.description />}

    \newcommand{\Technology${technology.camelCasedKey}Key}{${technology.key}}

    \newcommand{\Technology${technology.camelCasedKey}Recommendation}{<@tex.texify text=technology.recommendation.label />}

    \newcommand{\Technology${technology.camelCasedKey}Type}{<@tex.texify text=technology.technologyType.label />}
</#list>
