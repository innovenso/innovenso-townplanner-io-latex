% Town plan

\newcommand{\capability}[4][]
{
    \node [capability#4,#1] (#2) {#3};
}

\newcommand{\capabilityBox}[4][]
{
    \begin{pgfonlayer}{background}
        \node [capability#3,capabilityBox,#1,fit=#4] (#2) {};
    \end{pgfonlayer}
}


\newcommand{\capabilityBoxTitle}[4][]
{
    \node [capabilityTitle#4,#1] (#2) {#3};
}

\newcommand{\buildingBlock}[3][]
{
\node [buildingBlock,#1] (#2) {#3};
}

\newcommand{\workPackage}[3][]
{
\node [workPackage,#1] (#2) {#3};
}

\newcommand{\container}[6][]{
    \node[container,text width=#2, #1](#3){
        \begin{minipage}{#2}
            \begin{center}
                \textbf{#4}
                {[}container - #5{]}
                \rule{\textwidth}{.4pt}

                #6
            \end{center}
        \end{minipage}
    };
}

\newcommand{\system}[5][]{
    \node[system,text width=#2, #1](#3){
        \begin{minipage}{#2}
            \begin{center}
                \textbf{#4}
                \rule{\textwidth}{.4pt}

                #5
            \end{center}
        \end{minipage}
    };
}

\newcommand{\systemContext}[4][]
{
    \begin{pgfonlayer}{background}
        \node [systemContext,#1,fit=#4] (#2) {};
        \node [systemContextLabel, anchor=south west] at (#2.south west) {#3};

    \end{pgfonlayer}
}

\newcommand{\flow}[4][]
{
    \path [-{Triangle},flow,#1] (#2) -- node[above,sloped,relationLabel] {#4} (#3);
}

\newcommand{\biflow}[4][]
{
    \path [{Triangle}-{Triangle},flow,#1] (#2) -- node[above,sloped,relationLabel] {#4} (#3);
}

\newcommand{\belongsTo}[3][]
{
\path [-{Diamond},composition,#1] (#2) -- (#3);
}

\newcommand{\realizes}[3][]
{
\path [-{Triangle[open]},realization,#1] (#2) -- node[above,sloped,relationLabel] {realizes} (#3);
}

\newcommand{\implements}[3][]
{
\path [-{Triangle[open]},realization,#1] (#2) -- node[above,sloped,relationLabel] {implements} (#3);
}

\newcommand{\associatesWith}[3][]
{
\path [-,association,#1] (#2) -- (#3);
}

\newcommand{\actor}[4][1]
{
    \node [ person#1,#2 ] (#3) {#4};
}

\newcounter{SquadMemberCount}

\newcommand{\squad}[4][3]{ % radius, key, name, members
    \node(#2)[squad_name_node] at (0,0) {#3};
    \setcounter{SquadMemberCount}{0}
    \foreach \role [count=\memberindex] in {#4} {
        \stepcounter{SquadMemberCount}
    }
    \foreach \role [count=\memberindex] in {#4} {
        \node(\role)[person\memberindex,minimum size=\fpeval{#1/4}cm] at({360/\theSquadMemberCount * (\memberindex - 1)}:#1) {\role};
        \draw[-,very thin, dashed] ({360/\theSquadMemberCount * (\memberindex - 1)+15}:#1)
        arc ({360/\theSquadMemberCount * (\memberindex - 1)+15}:{360/\theSquadMemberCount * (\memberindex)-15}:#1);
        \path [-{Diamond}, thick] (\role) edge (#2);
    }
}

\newcommand{\stakeholderMap}[4]
{
\begin{figure}[h]
\centering

\begin{tikzpicture}[node distance=2mm and 2mm]
\node[stakeholderMapQuadrant, minimum height=#3cm, minimum width=#2cm] (high_interest_low_influence) { };
\node[stakeholderMapQuadrant, minimum height=#3cm, minimum width=#2cm, right=of high_interest_low_influence] (high_interest_high_influence) {  };
\node[stakeholderMapQuadrant, minimum height=#3cm, minimum width=#2cm, below=of high_interest_low_influence] (low_interest_low_influence) { };
\node[stakeholderMapQuadrant, minimum height=#3cm, minimum width=#2cm, right=of low_interest_low_influence] (low_interest_high_influence) { };

\node[anchor=south west, font=\tiny, inner sep=0mm, outer sep=0mm] at (high_interest_low_influence.north west) { keep completely informed };
\node[anchor=north west, font=\tiny, inner sep=0mm, outer sep=0mm] at (low_interest_low_influence.south west) { regular minimal contact };
\node[anchor=south east, font=\tiny, inner sep=0mm, outer sep=0mm] at (high_interest_high_influence.north east) { manage thoroughly };
\node[anchor=north east, font=\tiny, inner sep=0mm, outer sep=0mm] at (low_interest_high_influence.south east) { anticipate and meet needs };

\node[stakeholderMapPlusMinus, left=of high_interest_low_influence.north west] (more_interest){$+$};
\node[stakeholderMapPlusMinus, left=of low_interest_low_influence.south west] (less_interest){$-$};

\draw[<->] (less_interest) -- (more_interest) node [rotate=90, font=\small, anchor=south, midway, black] { interest };

\node[stakeholderMapPlusMinus, below=of low_interest_low_influence.south west] (less_influence){$-$};
\node[stakeholderMapPlusMinus, below=of low_interest_high_influence.south east] (more_influence){$+$};

\draw[<->] (less_influence) -- (more_influence) node [font=\small, anchor=north, midway, black] { influence };

\coordinate(min) at (low_interest_low_influence.south west);

#4
\end{tikzpicture}

%\caption{#1}
\end{figure}
}

\newcommand{\stakeholder}[5]
{
\node (#4) [person#1,stakeholderMapPerson,xshift=#2mm,yshift=#3mm] at (min) { #5 };
}



\newcommand{\ImpactTable}[7][] {
\begin{table}
\begin{tabularx}{\linewidth}{>{\parskip1ex}X@{\kern4\tabcolsep}>{\parskip1ex}X@{\kern4\tabcolsep}>{\parskip1ex}X}
\toprule
\hfil\bfseries #2
&
\hfil\bfseries #4
&
\hfil\bfseries #6
\\\cmidrule(r{3\tabcolsep}){1-1}\cmidrule(r{3\tabcolsep}){2-2}\cmidrule(l{-\tabcolsep}){3-3}

%% Added column
#3
&

%% Changed column
#5

&

%% Removed column
#7

\\\bottomrule
\end{tabularx}
\ifthenelse{\isempty{#1}}%
{}% if #1 is empty
{\caption{#1}}% if #1 is not empty
\end{table}}


<#list element.businessCapabilities as cap>
    \newcommand{\Capability${cap.camelCasedKey}Title}{${cap.title?replace("&","\\&")}}

    \newcommand{\Capability${cap.camelCasedKey}Key}{${cap.key}}

    \newcommand{\Capability${cap.camelCasedKey}TitleKey}{${cap.key}_title}

    \newcommand{\Capability${cap.camelCasedKey}}[1][]
    {
        \capability[#1] {\Capability${cap.camelCasedKey}Key} {\Capability${cap.camelCasedKey}Title}{${cap.level}}
    }

    \newcommand{\Capability${cap.camelCasedKey}Box}[2][]
    {
        \capabilityBox[#1] {\Capability${cap.camelCasedKey}Key} {${cap.level}} {#2}
    }

    \newcommand{\Capability${cap.camelCasedKey}BoxTitle}[1][]
    {
        \capabilityBoxTitle[#1] {\Capability${cap.camelCasedKey}TitleKey} {\Capability${cap.camelCasedKey}Title} {${cap.level}}
    }
</#list>

<#list element.buildingBlocks as block>
    \newcommand{\BuildingBlock${block.camelCasedKey}Title}{${block.title?replace("&","\\&")}}

    \newcommand{\BuildingBlock${block.camelCasedKey}Key}{${block.key}}

    \newcommand{\BuildingBlock${block.camelCasedKey}}[1][]
    {
    \buildingBlock[#1] {\BuildingBlock${block.camelCasedKey}Key} {\BuildingBlock${block.camelCasedKey}Title}
    }
</#list>

<#list element.levelZeroBusinessCapabilities as cap>
    <#list element.getBuildingBlocks(cap.key) as block>
    \newcommand{\CapabilityBuildingBlock${cap.camelCasedKey}${block.camelCasedKey}Key}{${cap.key}_${block.key}}

    \newcommand{\CapabilityBuildingBlock${cap.camelCasedKey}${block.camelCasedKey}}[1][]
    {
        \buildingBlock[#1] {\CapabilityBuildingBlock${cap.camelCasedKey}${block.camelCasedKey}Key} {\BuildingBlock${block.camelCasedKey}Title}
    }

    </#list>
</#list>

<#list element.projects as project>
    \newcommand{\WorkPackage${project.camelCasedKey}Title}{${project.title?replace("&","\\&")}}

    \newcommand{\WorkPackage${project.camelCasedKey}Key}{${project.key}}

    \newcommand{\WorkPackage${project.camelCasedKey}}[1][]
    {
    \workPackage[#1] {\WorkPackage${project.camelCasedKey}Key} {\WorkPackage${project.camelCasedKey}Title}
    }
</#list>

<#list element.systems as system>
    \newcommand{\System${system.camelCasedKey}Title}{${system.title?replace("&","\\&")}}

    \newcommand{\System${system.camelCasedKey}Description}
    {${system.description?replace("&","\\&")}}

    \newcommand{\System${system.camelCasedKey}Key}{${system.key}}

    \newcommand{\SystemContext${system.camelCasedKey}Key}{${system.key}_context}

    \newcommand{\System${system.camelCasedKey}}[2][]
    {
    \system[#1] {#2} {\System${system.camelCasedKey}Key} {\System${system.camelCasedKey}Title} {\System${system.camelCasedKey}Description}
    }

    \newcommand{\SystemContext${system.camelCasedKey}}[2][]
    {
    \systemContext[#1] {\SystemContext${system.camelCasedKey}Key} {\System${system.camelCasedKey}Title} {#2}
    }
</#list>

<#list element.containers as container>
    \newcommand{\Container${container.camelCasedKey}Title}{${container.title?replace("&","\\&")}}

    \newcommand{\Container${container.camelCasedKey}Description}{${container.description?replace("&","\\&")}}

    \newcommand{\Container${container.camelCasedKey}Technologies}{${container.technologyDescription?replace("&","\\&")}}

    \newcommand{\Container${container.camelCasedKey}Key}{${container.key}}

    \newcommand{\Container${container.camelCasedKey}}[2][]
    {
    \container[#1] {#2} {\Container${container.camelCasedKey}Key} {\Container${container.camelCasedKey}Title} {\Container${container.camelCasedKey}Technologies} {\Container${container.camelCasedKey}Description}
    }
</#list>

<#list element.businessPeople as person>
    \newcommand{\Person${person.camelCasedKey}Title}{${person.title?replace("&","\\&")}}

    \newcommand{\Person${person.camelCasedKey}Description}{${person.description?replace("&","\\&")}}

    \newcommand{\Person${person.camelCasedKey}Key}{${person.key}}

    \newcommand{\Person${person.camelCasedKey}}[1][]
    {
    \actor [${person_index % 20 + 1}] {#1} {\Person${person.camelCasedKey}Key} {\Person${person.camelCasedKey}Title}
    }
</#list>

<#list element.squads as squad>
    \newcommand{\Squad${squad.camelCasedKey}}[1][3]
    {
    \squad [#1] {${squad.key}} {${squad.title?replace("&","\\&")}} {<#list squad.getMembers() as member>${member.title?replace("&","\\&")}<#if member_has_next>,</#if></#list>}
    }
</#list>

<#list element.projects as project>
    \newcommand{\StakeholderMap${project.camelCasedKey}}
    {
        \stakeholderMap{${project.title} - stakeholder map}{6}{2.2}{
            <#list project.stakeholderRelationships as rel>
                \stakeholder{${rel_index % 20 + 1}}{${(12.2 / 10 * rel.influence)?round}}{${(4.6 / 10 * rel.interest)?round}}{${rel.source.key}}{${rel.source.title}}
            </#list>
        }
    }
</#list>

% project information model impact map

<#list element.projects as project>
    \newcommand{\Project${project.camelCasedKey}InformationModelImpact}[4][] {
        \ImpactTable[#1]{#2}{
            % added
            <#list project.addedDataEntities as entity>
                ${entity.title}  <#if entity.businessCapability??>\textsubscript{$\in$ ${entity.businessCapability.title}}</#if> \par

            </#list>
        }{#3}{
            % changed
            <#list project.changedDataEntities as entity>
                ${entity.title} <#if entity.businessCapability??>\textsubscript{$\in$ ${entity.businessCapability.title}}</#if> \par

            </#list>
        }{#4}{
            % removed
            <#list project.removedDataEntities as entity>
                ${entity.title} <#if entity.businessCapability??>\textsubscript{$\in$ ${entity.businessCapability.title}}</#if> \par

            </#list>
        }
    }
</#list>

