<#import "lib/texify.ftl" as tex>

<#list model.buildingBlocks as bb>
    \newcommand{\BuildingBlock${bb.camelCasedKey}Title}{<@tex.texify text=bb.title />}

    \newcommand{\BuildingBlock${bb.camelCasedKey}Description}{<@tex.texify text=bb.description />}

    \newcommand{\BuildingBlock${bb.camelCasedKey}Key}{${bb.key}}

    \newcommand{\BuildingBlock${bb.camelCasedKey}TitleKey}{${bb.key}_title}

    \newcommand{\BuildingBlock${bb.camelCasedKey}Context}{\includegraphics{${bb.key}-context.eps}}

    \newcommand{\BuildingBlock${bb.camelCasedKey}ContextDrawing}{\drawing[\BuildingBlock${bb.camelCasedKey}Title]{${bb.key}-context.eps}}

    \newcommand{\BuildingBlock${bb.camelCasedKey}Overview}{\includegraphics{${bb.key}.eps}}

    \newcommand{\BuildingBlock${bb.camelCasedKey}OverviewSimple}{\includegraphics{${bb.key}-simple.eps}}

    \newcommand{\BuildingBlock${bb.camelCasedKey}OverviewDrawing}{\drawing[\BuildingBlock${bb.camelCasedKey}Title]{${bb.key}.eps}}

    \newcommand{\BuildingBlock${bb.camelCasedKey}OverviewDrawingSimple}{\drawing[\BuildingBlock${bb.camelCasedKey}Title]{${bb.key}-simple.eps}}

</#list>
