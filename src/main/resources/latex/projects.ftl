<#import "lib/texify.ftl" as tex>

<#list model.projects as project>
    \newcommand{\Project${project.camelCasedKey}Title}{<@tex.texify text=project.title />}

    \newcommand{\Project${project.camelCasedKey}Description}{<@tex.texify text=project.description />}

    \newcommand{\Project${project.camelCasedKey}Key}{${project.key}}

    \newcommand{\Project${project.camelCasedKey}TitleKey}{${project.key}_title}
</#list>
