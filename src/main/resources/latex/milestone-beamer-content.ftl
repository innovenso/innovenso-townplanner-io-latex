<#import  "lib/integrationslides.ftl" as s>
<#import  "lib/integrationtable.ftl" as it>
<#import  "lib/securityconcerns.ftl" as sc>
<#import  "lib/securityimpacts.ftl" as si>
<#import  "lib/nemawashi.ftl" as nema>
<#import "lib/constraints.ftl" as con>
<#import "lib/swot.ftl" as swot>
<#import "lib/functionalRequirements.ftl" as func>
<#import "lib/qar.ftl" as qar>
<#import "lib/decisiontable.ftl" as dect>

\usetheme{${theme}}

\institute{${institution}}
\date{\today}
\version{1.0.0}

\definecolor{delijngeel}{RGB}{255,221,0}
\definecolor{delijnlichtgrijs}{RGB}{204,204,204}
\definecolor{tertiarylightgray}{RGB}{153,153,153}
\definecolor{delijngrijs}{RGB}{102,102,102}
\definecolor{delijnwit}{RGB}{255,255,255}
\definecolor{delijnzwart}{RGB}{0,0,0}

\definecolor{technischrood}{RGB}{187,0,34}
\definecolor{technischoranje}{RGB}{238,136,34}
\definecolor{technischgeel}{RGB}{255,204,17}
\definecolor{technischlichtgroen}{RGB}{187,221,0}freemarker macro
\definecolor{technischgroen}{RGB}{34,153,34}
\definecolor{technischblauw}{RGB}{17,153,221}
\definecolor{technischdonkerblauw}{RGB}{0,68,187}
\definecolor{technischpaars}{RGB}{153,17,153}

\definecolor{gold}{RGB}{255,215,0}
\definecolor{silver}{RGB}{192,192,192}
\definecolor{bronze}{RGB}{205,127,50}

\usepackage{multicol}
\usepackage[nochapter]{vhistory}

\include{townplan-style}
\include{enterprises}
\include{capabilities}
\include{building-blocks}
\include{systems}
\include{actors}
\include{projects}
\include{containers}
\include{flows}
\include{integrations}
\include{milestones}
\include{technologies}freemarker macro
\include{decisions}

\title{${model.title}}
\subtitle{Architecture Review}
\author{Jurgen Lust}

\begin{document}
\maketitle

\agenda

\section{Objectief}

\include{objectief}

<@nema.nemawashi model=model />

\section{Context}

\include{context_diagram}

\subsection{Impact on capabilities, building blocks and information model}
\begin{frame}{Capability Map}
\Milestone${model.camelCasedKey}CapabilityImpactDiagramDrawing
\end{frame}

\begin{frame}{Architecture Building Blocks}
\Milestone${model.camelCasedKey}BuildingBlockImpactDiagramDrawing
\end{frame}

<#if model.constraints?size!=0>
    \subsection{Constraints and Risks}

    <@con.constraints constraints=model.constraints />
    <@swot.swot conceptModel=model />
</#if>

\section{Requirements}

\subsection{Functional and non-functional requirements}

\symbolBackground[tertiarylightgray]{\faBusinessTime} {
<@func.functionalRequirements conceptModel=model />
}

\symbolBackground[tertiarylightgray]{\faCogs} {
<@qar.qar conceptModel=model />
}

<#if model.securityConcerns?size != 0 || model.securityImpacts?size != 0>

\subsection{Security \& Compliance}

\begin{frame}[t]{Security \& Compliance}
\tikzstyle{impact_box}=[rectangle, inner sep=1mm, align=center, font=\tiny, text width=5mm, text centered, minimum height=4mm]
\tikzstyle{description_box}=[text width=8cm, font=\scriptsize, inner sep=1mm]
\tikzstyle{low}=[impact_box, fill=green!40, text=black]
\tikzstyle{medium}=[impact_box, fill=green, text=black]
\tikzstyle{high}=[impact_box, fill=yellow, text=black]
\tikzstyle{xhigh}=[impact_box, fill=red, text=white]

\begin{tikzpicture}[node distance=1mm]
<@sc.securityconcerns concerns=model.securityConcerns />
<@si.securityimpacts impacts=model.securityImpacts />
\end{tikzpicture}
\end{frame}
</#if>

\section{Solution}

<@dect.decision_table conceptModel=model />

\include{solution}

\section{Design}

\subsection{Overview}

\begin{frame}{As Is}
\Milestone${model.camelCasedKey}AsIsDiagramDrawing
\end{frame}

\begin{frame}{To Be}
\Milestone${model.camelCasedKey}ToBeDiagramDrawing
\end{frame}

\begin{frame}{Systems and Integrations}
\Milestone${model.camelCasedKey}IntegrationMapDiagramDrawing
\end{frame}

\subsection{Integrations}

<#if model.addedItSystemIntegrations?size != 0>
    \subsection{New Integrations}
    <@it.integration_table integrations=model.addedItSystemIntegrations title="New Integrations" />
    <@s.integration_slides integrations=model.addedItSystemIntegrations />
</#if>

<#if model.changedItSystemIntegrations?size != 0>
    \subsection{Updated Integrations}
    <@it.integration_table integrations=model.changedItSystemIntegrations title="Changed Integrations" />
    <@s.integration_slides integrations=model.changedItSystemIntegrations />
</#if>

<#if model.removedItSystemIntegrations?size != 0>
    \subsection{Removed Integrations}
    <@it.integration_table integrations=model.removedItSystemIntegrations title="Removed Integrations" />
    <@s.integration_slides integrations=model.removedItSystemIntegrations />
</#if>

\section{Impact}

\symbolBackground[tertiarylightgray]{\faEuroSign} {
    \include{financial_impact}
}

\symbolBackground[tertiarylightgray]{\faSitemap} {
    \include{dependencies}
}

\section{Actions}

\include{actions}

%    \section*{Revisies}
%
%    \begin{frame}{Revisies}
%        \include{revision_log}
%    \end{frame}


\end{document}