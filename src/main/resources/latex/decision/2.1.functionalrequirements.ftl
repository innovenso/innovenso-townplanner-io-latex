<#import "../lib/texify.ftl" as tex>
<#list model.functionalRequirements as req>
    \begin{frame}{<@tex.texify text=req.title />}{Functional Requirement}
    \alert{<@tex.texify text=req.weight.label />}

    <@tex.texify text=req.description />
    \end{frame}
</#list>
