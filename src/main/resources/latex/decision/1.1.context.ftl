<#import "../lib/texify.ftl" as tex>
\begin{frame}{What are we deciding?}{<@tex.texify text=model.title />}
<@tex.texify text=model.description />
\end{frame}

<#list model.furtherDescriptions as description>
    \begin{frame}{What are we deciding? - continued}{<@tex.texify text=model.title />}
        <@tex.texify text=description />
    \end{frame}
</#list>