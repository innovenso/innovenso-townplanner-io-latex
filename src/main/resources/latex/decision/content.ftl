<#import "../lib/texify.ftl" as tex>

\usetheme{${theme}}

\institute{${institution}}

\usepackage{multicol}
\usepackage[nochapter]{vhistory}

\include{townplan-style}
\include{enterprises}
\include{capabilities}
\include{building-blocks}
\include{systems}
\include{actors}
\include{projects}
\include{containers}
\include{flows}
\include{integrations}
\include{milestones}
\include{technologies}
\include{decisions}

\include{0.title}

\begin{document}
\maketitle

\agenda

\section{Context}

<#if model.hasContext()>
    \subsection{Background}
    \include{1.1.context}
    \include{1.2.current}
    \include{1.3.goals}
    \include{1.4.assumptions}
</#if>


<#if model.hasNemawashi()>
    \subsection{Nemawashi}
    \include{1.5.nemawashi}
</#if>

\section{Analysis}

\subsection{Functional Requirements}

\symbolBackground[tertiarylightgray]{\faBusinessTime} {
\include{2.1.functionalrequirements}
}

\subsection{Quality Attribute Requirements}

\symbolBackground[tertiarylightgray]{\faCogs} {
\include{2.2.qar}
}

\subsection{Constraints}
\include{2.3.constraints}


\section{Options Considered}

\begin{frame}{Options Considered}
    \begin{itemize}
    <#list model.options as option>
        \item <@tex.texify text=option.title />
    </#list>
    \end{itemize}
\end{frame}

<#list model.options as option>
    \include{3.${option?counter}.1.${model.getOptionKey(option)}.description}
    \include{3.${option?counter}.2.${model.getOptionKey(option)}.swot}

    \symbolBackground[tertiarylightgray]{\faEuroSign} {
    \include{3.${option?counter}.3.${model.getOptionKey(option)}.cost}
    }

    \include{3.${option?counter}.4.${model.getOptionKey(option)}.match}
</#list>


\section{Decision}

\include{4.1.decisioncomparison}


\include{4.2.outcome}

\section{Followup}
\include{5.1.consequences}

\end{document}