<#import "../lib/texify.ftl" as tex>
<#if model.currentStates?has_content>
    \subsection{Current Conditions}

    <#list model.currentStates as currentState>
        \begin{frame}{<@tex.texify text=currentState.title />}{Current Condition}
        <@tex.texify text=currentState.description />
        \end{frame}
    </#list>
</#if>
