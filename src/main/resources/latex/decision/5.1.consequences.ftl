<#import "../lib/texify.ftl" as tex>
<#if model.consequences?has_content>
    \subsection{Consequences}

    <#list model.consequences as consequence>
        \begin{frame}{<@tex.texify text=consequence.title />}{Consequence}
        <@tex.texify text=consequence.description />
        \end{frame}
    </#list>
</#if>
