<#import "../lib/texify.ftl" as tex>

\begin{frame}{Nemawashi}

\begin{columns}
\begin{column}{6cm}
<#if model.nemawashiOwners?has_content>
    \alert{Responsible}:
    \begin{itemize}
        <#list model.nemawashiOwners as nema>
            \item <@tex.texify text=nema.title />
        </#list>
    \end{itemize}
</#if>

<#if model.nemawashiApprovers?has_content>
    \alert{Accountable}:
    \begin{itemize}
        <#list model.nemawashiApprovers as nema>
            \item <@tex.texify text=nema.title />
        </#list>
    \end{itemize}
</#if>

\end{column}
\begin{column}{6cm}
<#if model.nemawashiStakeholders?has_content>
    \alert{Consulted and/or informed}:
    \begin{itemize}
        <#list model.nemawashiStakeholders as nema>
            \item <@tex.texify text=nema.title />
        </#list>
    \end{itemize}
</#if>
\end{column}
\end{columns}
\end{frame}
