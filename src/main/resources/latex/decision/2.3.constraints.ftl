<#import "../lib/texify.ftl" as tex>
<#list model.constraints as req>
    \begin{frame}{<@tex.texify text=req.title />}{Constraint}
    \alert{<@tex.texify text=req.weight.label />}

    <@tex.texify text=req.description />
    \end{frame}
</#list>
