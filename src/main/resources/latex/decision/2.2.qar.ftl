<#import "../lib/texify.ftl" as tex>
<#list model.qars as req>
    \begin{frame}{<@tex.texify text=req.title />}{Quality Attribute Requirement}
    \alert{<@tex.texify text=req.weight.label />}

    \begin{description}[labelwidth=4cm]
    <#if req.sourceOfStimulus?has_content>
        \item[Source of stimulus] <@tex.texify text=req.sourceOfStimulus />
    </#if>
    <#if req.stimulus?has_content>
        \item[Stimulus] <@tex.texify text=req.stimulus />
    </#if>
    <#if req.environment?has_content>
        \item[Environment] <@tex.texify text=req.environment />
    </#if>
    <#if req.response?has_content>
        \item[Response] <@tex.texify text=req.response />
    </#if>
    <#if req.responseMeasure?has_content>
        \item[Response Measure] <@tex.texify text=req.responseMeasure />
    </#if>
    \end{description}
    \end{frame}
</#list>
