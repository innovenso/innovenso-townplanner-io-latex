<#import "../lib/texify.ftl" as tex>
<#if model.assumptions?has_content>
    \subsection{Assumptions}

    <#list model.assumptions as assumption>
        \begin{frame}{<@tex.texify text=assumption.title />}{Assumption}
        <@tex.texify text=assumption.description />
        \end{frame}
    </#list>
</#if>
