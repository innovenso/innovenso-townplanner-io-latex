<#import "../lib/texify.ftl" as tex>
\begin{frame}{Option comparison}
\begin{tiny}
\begin{table}
\bgroup
\def\arraystretch{1.5}%
\begin{tabular}{  @{}p{7cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}@{}  }
\toprule
option & \multicolumn{4}{c}{scores} \\
\cmidrule(r){1-1}\cmidrule(l){2-5}
& functional & qar & constraints & total (\%) \\
\midrule
<#list model.options as option>
    <@tex.texify text=option.title /> & ${option.totalFunctionalScore}/${option.maximumTotalFunctionalScore} & ${option.totalQualityAttributeScore}/${option.maximumTotalQualityAttributeScore} & ${option.totalConstraintScore}/${option.maximumTotalConstraintScore} & ${option.totalScoreAsPercentage}\% \\
    <#if option?counter % 10 == 0 && option?counter != 0 && option?has_next>
        \bottomrule
        \end{tabular}
        \egroup
        \end{table}
        \end{tiny}
        \end{frame}

        \begin{frame}{Option comparison}
        \begin{tiny}
        \begin{table}
        \bgroup
        \def\arraystretch{1.5}%
        \begin{tabular}{  @{}p{7cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}@{}  }
        \toprule
        option & \multicolumn{4}{c}{scores} \\
        \cmidrule(r){1-1}\cmidrule(l){2-5}
        & functional & qar & constraints & total (\%) \\
        \midrule
    </#if>
</#list>
\bottomrule
\end{tabular}
\egroup
\end{table}
\end{tiny}
\end{frame}