<#import "../lib/texify.ftl" as tex>
<#list option.costFiscalYears as fiscalYear>
    \begin{frame}{Cost Estimation - ${fiscalYear}}{<@tex.texify text=option.title />}
    \begin{tiny}
    \begin{table}
    \bgroup
    \def\arraystretch{1.5}%
    \begin{tabular}{  @{}p{4cm}p{1.5cm}rrp{5cm}@{}  }
    \toprule
    & & Capex & Opex & Remarks\\
    \midrule
    <#list model.getCosts(option, fiscalYear) as cost>
        <@tex.texify text=cost.title /> & <@tex.texify text=cost.category /> & ${cost.capex} & ${cost.opex} & <@tex.texify text=cost.remarks /> \\
        <#if cost?counter % 8 == 0 && cost?counter != 0 && cost?has_next>
            \bottomrule
            \end{tabular}
            \egroup
            \end{table}
            \end{tiny}
            \end{frame}

            \begin{frame}{Cost Estimation - ${fiscalYear}}{<@tex.texify text=option.title />}
            \begin{tiny}
            \begin{table}
            \bgroup
            \def\arraystretch{1.5}%
            \begin{tabular}{  @{}p{4cm}p{1.5cm}rrp{5cm}@{}  }
            \toprule
            & & Capex & Opex & Remarks\\
            \midrule
        </#if>
    </#list>
    \midrule
    total &  & ${model.getTotalCost(option,fiscalYear).capex} & ${model.getTotalCost(option,fiscalYear).opex} &  \\
    \bottomrule
    \end{tabular}
    \egroup
    \end{table}
    \end{tiny}
    \end{frame}
</#list>