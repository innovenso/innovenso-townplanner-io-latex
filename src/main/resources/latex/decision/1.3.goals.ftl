<#import "../lib/texify.ftl" as tex>
<#if model.goals?has_content>
    \subsection{Goals}

    <#list model.goals as goal>
        \begin{frame}{<@tex.texify text=goal.title />}{Goal}
        <@tex.texify text=goal.description />
        \end{frame}
    </#list>
</#if>
