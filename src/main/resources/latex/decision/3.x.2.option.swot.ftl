<#import "../lib/texify.ftl" as tex>
\begin{frame}{SWOT}{<@tex.texify text=option.title />}
\begin{tiny}
\begin{tcbraster}[raster columns=2, boxrule=0mm, arc=0mm]
\begin{tcolorbox}[equal height group=A, size=fbox, colback=swotS!60, colframe=swotS!80!black, title=\textsc{strengths}]
<#if model.getStrengths(option)?has_content>
    \begin{itemize}
        <#list model.getStrengths(option) as swot>
            \item <@tex.texify text=swot.description />
        </#list>
    \end{itemize}
</#if>
\end{tcolorbox}
\begin{tcolorbox}[equal height group=A, size=fbox, colback=swotW!60, colframe=swotW!80!black, title=\textsc{weaknesses}]
<#if model.getWeaknesses(option)?has_content>
    \begin{itemize}
    <#list model.getWeaknesses(option) as swot>
        \item <@tex.texify text=swot.description />
    </#list>
    \end{itemize}
</#if>
\end{tcolorbox}
\begin{tcolorbox}[equal height group=B, size=fbox, colback=swotO!60, colframe=swotO!80!black, title=\textsc{opportunities}]
<#if model.getOpportunities(option)?has_content>
    \begin{itemize}
    <#list model.getOpportunities(option) as swot>
        \item <@tex.texify text=swot.description />
    </#list>
    \end{itemize}
</#if>
\end{tcolorbox}
\begin{tcolorbox}[equal height group=B, size=fbox, colback=swotT!60, colframe=swotT!80!black, title=\textsc{threats}]
<#if model.getThreats(option)?has_content>
    \begin{itemize}
    <#list model.getThreats(option) as swot>
        \item <@tex.texify text=swot.description />
    </#list>
    \end{itemize}
</#if>
\end{tcolorbox}
\end{tcbraster}
\end{tiny}
\end{frame}