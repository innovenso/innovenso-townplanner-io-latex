<#import "../lib/texify.ftl" as tex>
\begin{frame}{Requirements match}{<@tex.texify text=option.title />}
\begin{tiny}
\begin{table}
\bgroup
\def\arraystretch{1.5}%
\begin{tabular}{  @{}p{6.5cm}p{2cm}rrrr@{}  }
\toprule
\multicolumn{2}{c}{requirements} & \multicolumn{4}{c}{expectations} \\
\cmidrule(r){1-2}\cmidrule(l){3-6}
& weight & exceeds & meets & approaches & below \\
\midrule

<#assign reqcount = 0>

<#if option.functionalRequirements?has_content>
    \textbf{functional requirements} & & & & & \\
    <#list option.functionalRequirements as req>
        <@tex.texify text=req.title /> & <@tex.texify text=req.weight.label /> & ${model.getExceedColumn(option, req)} & ${model.getMeetColumn(option, req)} & ${model.getAlmostColumn(option, req)} & ${model.getLackColumn(option, req)}  \\
        <#assign reqcount = reqcount + 1 >

        <#if reqcount % 8 == 0 && reqcount != 0 && req?has_next>
            \bottomrule
            \end{tabular}
            \egroup
            \end{table}
            \end{tiny}
            \end{frame}

            \begin{frame}{Requirements match}{<@tex.texify text=option.title />}
            \begin{tiny}
            \begin{table}
            \bgroup
            \def\arraystretch{1.5}%
            \begin{tabular}{  @{}p{6.5cm}p{2cm}rrrr@{}  }
            \toprule
            \multicolumn{2}{c}{requirements} & \multicolumn{4}{c}{expectations} \\
            \cmidrule(r){1-2}\cmidrule(l){3-6}
            & weight & exceeds & meets & approaches & below \\
            \midrule
            \textbf{functional requirements} & & & & & \\
        </#if>
    </#list>
</#if>

<#if option.qualityAttributes?has_content>
    \textbf{quality attribute requirements} & & & & & \\
    <#list option.qualityAttributes as req>
        <@tex.texify text=req.title /> & <@tex.texify text=req.weight.label /> & ${model.getExceedColumn(option, req)} & ${model.getMeetColumn(option, req)} & ${model.getAlmostColumn(option, req)} & ${model.getLackColumn(option, req)}  \\
        <#assign reqcount = reqcount + 1>

        <#if reqcount % 8 == 0 && reqcount != 0 && req?has_next>
            \bottomrule
            \end{tabular}
            \egroup
            \end{table}
            \end{tiny}
            \end{frame}

            \begin{frame}{Requirements match}{<@tex.texify text=option.title />}
            \begin{tiny}
            \begin{table}
            \bgroup
            \def\arraystretch{1.5}%
            \begin{tabular}{  @{}p{6.5cm}p{2cm}rrrr@{}  }
            \toprule
            \multicolumn{2}{c}{requirements} & \multicolumn{4}{c}{expectations} \\
            \cmidrule(r){1-2}\cmidrule(l){3-6}
            & weight & exceeds & meets & approaches & below \\
            \midrule
            \textbf{quality attribute requirements} & & & & & \\
        </#if>
    </#list>
</#if>

<#if option.constraints?has_content>
    \textbf{constraints} & & & & & \\
    <#list option.constraints as req>
        <@tex.texify text=req.title /> & <@tex.texify text=req.weight.label /> & ${model.getExceedColumn(option, req)} & ${model.getMeetColumn(option, req)} & ${model.getAlmostColumn(option, req)} & ${model.getLackColumn(option, req)}  \\
        <#assign reqcount = reqcount + 1>

        <#if reqcount % 8 == 0 && reqcount != 0 && req?has_next>
            \bottomrule
            \end{tabular}
            \egroup
            \end{table}
            \end{tiny}
            \end{frame}

            \begin{frame}{Requirements match}{<@tex.texify text=option.title />}
            \begin{tiny}
            \begin{table}
            \bgroup
            \def\arraystretch{1.5}%
            \begin{tabular}{  @{}p{6.5cm}p{2cm}rrrr@{}  }
            \toprule
            \multicolumn{2}{c}{requirements} & \multicolumn{4}{c}{expectations} \\
            \cmidrule(r){1-2}\cmidrule(l){3-6}
            & weight & exceeds & meets & approaches & below \\
            \midrule
            \textbf{constraints} & & & & & \\
        </#if>

    </#list>
</#if>
\bottomrule
\end{tabular}
\egroup
\end{table}
\end{tiny}
\end{frame}
