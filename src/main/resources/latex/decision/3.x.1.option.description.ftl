<#import "../lib/texify.ftl" as tex>
\subsection{<@tex.texify text=option.title />}

\begin{frame}{<@tex.texify text=option.title />}
<@tex.texify text=option.description />
\end{frame}
