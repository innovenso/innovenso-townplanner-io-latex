package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexSystemWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexSystemWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "systems";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "systems";
	}
}
