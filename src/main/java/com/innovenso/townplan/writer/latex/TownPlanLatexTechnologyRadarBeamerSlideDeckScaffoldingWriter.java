package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.writer.pipeline.ClassPathResourceFileProvider;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Log4j2
public class TownPlanLatexTechnologyRadarBeamerSlideDeckScaffoldingWriter implements TownPlanLatexFileWriter {
	private final ClassPathResourceFileProvider fileProvider;
	private final File targetBaseDirectory;

	public TownPlanLatexTechnologyRadarBeamerSlideDeckScaffoldingWriter(
			@NonNull final LatexWriterConfiguration configuration) {
		this.targetBaseDirectory = new File(configuration.getTargetBaseDirectory(), "src/decks/techradar");
		this.fileProvider = new ClassPathResourceFileProvider();
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, List<File> renderedFiles) {

		List.of("presentation.tex", "handouts.tex", "handouts_with_notes.tex").stream().map(this::write)
				.forEach(renderedFiles::add);
	}

	private File write(@NonNull final String fileName) {
		final Path outputPath = Paths.get(targetBaseDirectory.getAbsolutePath(), fileName);
		final File directory = outputPath.getParent().toFile();
		directory.mkdirs();
		final File outputFile = outputPath.toFile();
		fileProvider.writeFile(outputFile, "/latex/" + fileName);
		log.debug("writing LaTeX file {}", outputFile.getAbsolutePath());
		return outputFile;
	}
}
