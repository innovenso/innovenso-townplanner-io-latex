package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.writer.model.DecisionPresentationModel;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class TownPlanLatexDecisionBeamerSlideDeckContentWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexDecisionBeamerSlideDeckContentWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/decks");
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		townPlan.getElements(Decision.class).forEach(decision -> {
			final DecisionPresentationModel presentationModel = new DecisionPresentationModel(townPlan, decision);
			final Map<String, Object> model = Map.of("model", presentationModel, "theme",
					writerConfiguration.getTheme(), "institution", writerConfiguration.getInstitution());

			final File milestoneDirectory = new File(targetBaseDirectory, decision.getKey());
			milestoneDirectory.mkdirs();
			List.of("content", "0.title", "1.1.context", "1.2.current", "1.3.goals", "1.4.assumptions", "1.5.nemawashi",
					"2.1.functionalrequirements", "2.2.qar", "2.3.constraints", "4.1.decisioncomparison", "4.2.outcome",
					"5.1.consequences")
					.forEach(filename -> writeSection(model, renderedFiles, milestoneDirectory, filename, filename));

			final AtomicInteger counter = new AtomicInteger(1);
			presentationModel.getOptions().forEach(option -> {
				final Map<String, Object> optionModel = Map.of("model", presentationModel, "option", option);
				final int count = counter.getAndIncrement();
				writeSection(optionModel, renderedFiles, milestoneDirectory, "3.x.1.option.description",
						"3." + count + ".1." + presentationModel.getOptionKey(option) + ".description");
				writeSection(optionModel, renderedFiles, milestoneDirectory, "3.x.2.option.swot",
						"3." + count + ".2." + presentationModel.getOptionKey(option) + ".swot");
				writeSection(optionModel, renderedFiles, milestoneDirectory, "3.x.3.option.cost",
						"3." + count + ".3." + presentationModel.getOptionKey(option) + ".cost");
				writeSection(optionModel, renderedFiles, milestoneDirectory, "3.x.4.option.match",
						"3." + count + ".4." + presentationModel.getOptionKey(option) + ".match");
			});
		});
	}

	@Override
	protected String getLatexTemplateName() {
		return "not used";
	}

	@Override
	protected String getOutputFileName(@NonNull TownPlan townPlan) {
		return "not used";
	}

	private void writeSection(@NonNull final Map<String, Object> model, @NonNull final List<File> renderedFiles,
			@NonNull final File milestoneDirectory, @NonNull final String templateName,
			@NonNull final String outputName) {
		Optional<File> latexOutputFile = freemarkerRenderer.write(model, "decision/" + templateName + ".ftl");
		latexOutputFile.ifPresent(render -> {
			final File resultingFile = new File(milestoneDirectory, outputName + ".tex");
			try {
				FileUtils.copyFile(render, resultingFile);
				renderedFiles.add(resultingFile);
			} catch (IOException e) {
				log.error(e);
			}
		});
	}
}
