package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexBusinessCapabilityWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexBusinessCapabilityWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "capabilities";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "capabilities";
	}
}
