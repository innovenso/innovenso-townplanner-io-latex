package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.writer.pipeline.ClassPathResourceFileProvider;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

@Log4j2
public class TownPlanLatexBeamerThemeWriter implements TownPlanLatexFileWriter {
	private final ClassPathResourceFileProvider fileProvider;
	private final File targetBaseDirectory;

	public TownPlanLatexBeamerThemeWriter(@NonNull final LatexWriterConfiguration configuration) {
		this.targetBaseDirectory = new File(configuration.getTargetBaseDirectory(), "theme");
		this.fileProvider = new ClassPathResourceFileProvider();
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, List<File> renderedFiles) {
		List.of("beamercolorthemetownplanner.sty", "beamerfontthemetownplanner.sty", "beamerinnerthemetownplanner.sty",
				"beamerouterthemetownplanner.sty", "beamerthemetownplanner.sty", "beamercolorthemenapoleon.sty",
				"beamerfontthemenapoleon.sty", "beamerinnerthemenapoleon.sty", "beamerouterthemenapoleon.sty",
				"beamerthemenapoleon.sty", "beamercolorthemeinnovenso.sty", "beamerfontthemeinnovenso.sty",
				"beamerinnerthemeinnovenso.sty", "beamerouterthemeinnovenso.sty", "beamerthemeinnovenso.sty",
				"handoutWithNotes.sty", "lib/pgf-pie.sty").stream().map(this::write).forEach(renderedFiles::add);
		IntStream.rangeClosed(1, 20).mapToObj(number -> "images/" + String.valueOf(number) + ".pdf").map(this::write)
				.forEach(renderedFiles::add);
		List.of("images/vlaanderen.pdf", "images/delijn.pdf", "images/logo.pdf", "images/napoleon-negative.eps",
				"images/tagline_black.pdf", "images/tagline_white.pdf").stream().map(this::write)
				.forEach(renderedFiles::add);
	}

	private File write(@NonNull final String fileName) {
		final Path outputPath = Paths.get(targetBaseDirectory.getAbsolutePath(), fileName);
		final File directory = outputPath.getParent().toFile();
		directory.mkdirs();
		final File outputFile = outputPath.toFile();
		fileProvider.writeFile(outputFile, "/latex/theme/" + fileName);
		log.debug("writing LaTeX theme file {}", outputFile.getAbsolutePath());
		return outputFile;
	}
}
