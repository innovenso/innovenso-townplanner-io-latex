package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexEnterpriseWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexEnterpriseWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "enterprises";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "enterprises";
	}
}
