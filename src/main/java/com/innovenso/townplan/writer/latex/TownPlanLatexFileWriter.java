package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;

import java.io.File;
import java.util.List;

public interface TownPlanLatexFileWriter {
	void write(TownPlan townPlan, List<File> renderedFiles);
}
