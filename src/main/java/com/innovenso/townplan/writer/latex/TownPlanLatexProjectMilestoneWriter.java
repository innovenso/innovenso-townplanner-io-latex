package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexProjectMilestoneWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexProjectMilestoneWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "milestones";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "milestones";
	}
}
