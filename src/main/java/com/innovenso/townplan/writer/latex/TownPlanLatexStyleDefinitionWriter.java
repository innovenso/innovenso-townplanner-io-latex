package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexStyleDefinitionWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexStyleDefinitionWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "townplan-tikz-style";
	}

	@Override
	public String getOutputFileName(@NonNull final TownPlan townPlan) {
		return "townplan-style";
	}
}
