package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.pipeline.AbstractTownPlanWriter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.List;

@Log4j2
public class TownPlanLatexWriter extends AbstractTownPlanWriter {
	private final List<TownPlanLatexFileWriter> writers;
	private final LatexWriterConfiguration writerConfiguration;

	public TownPlanLatexWriter(@NonNull final String targetBasePath, @NonNull final String theme,
			@NonNull final String institution, @NonNull final AssetRepository assetRepository) {
		super(assetRepository, ContentOutputType.LATEX);
		final File targetBaseDirectory = new File(targetBasePath, "latex");
		targetBaseDirectory.mkdirs();
		this.writerConfiguration = LatexWriterConfiguration.builder().theme(theme).institution(institution)
				.targetBaseDirectory(targetBaseDirectory).repository(assetRepository).build();
		this.writers = List.of(new TownPlanLatexEnterpriseWriter(writerConfiguration),
				new TownPlanLatexStyleDefinitionWriter(writerConfiguration),
				new TownPlanLatexDiagramWriter(targetBasePath, assetRepository),
				new TownPlanLatexBeamerThemeWriter(writerConfiguration),
				new TownPlanLatexBusinessCapabilityWriter(writerConfiguration),
				new TownPlanLatexArchitectureBuildingBlockWriter(writerConfiguration),
				new TownPlanLatexSystemWriter(writerConfiguration),
				new TownPlanLatexBusinessActorWriter(writerConfiguration),
				new TownPlanLatexProjectWriter(writerConfiguration),
				new TownPlanLatexContainerWriter(writerConfiguration), new TownPlanLatexFlowWriter(writerConfiguration),
				new TownPlanLatexSystemIntegrationWriter(writerConfiguration),
				new TownPlanLatexProjectMilestoneWriter(writerConfiguration),
				new TownPlanLatexTechnologyWriter(writerConfiguration),
				new TownPlanLatexProjectMilestoneBeamerSlideDeckContentWriter(writerConfiguration),
				new TownPlanLatexProjectMilestoneBeamerSlideDeckScaffoldingWriter(writerConfiguration),
				new TownPlanLatexTechnologyRadarBeamerSlideDeckContentWriter(writerConfiguration),
				new TownPlanLatexTechnologyRadarBeamerSlideDeckScaffoldingWriter(writerConfiguration),
				new TownPlanLatexBuildFileWriter(writerConfiguration),
				new TownPlanLatexDecisionWriter(writerConfiguration),
				new TownPlanLatexDecisionBeamerSlideDeckContentWriter(writerConfiguration),
				new TownPlanLatexDecisionBeamerSlideDeckTikzWriter(writerConfiguration),
				new TownPlanLatexDecisionBeamerSlideDeckScaffoldingWriter(writerConfiguration));
	}

	@Override
	public void writeFiles(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		writers.forEach(writer -> writer.write(townPlan, renderedFiles));
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> list, @NonNull String s) {
		log.warn("LaTeX export does not supported rendering a single element, rendering full town plan instead");
		writeFiles(townPlan, list);
	}

	@Override
	public void setTargetBaseDirectory(@NonNull final File targetBaseDirectory) {
	}

	@Override
	public File getTargetBaseDirectory() {
		return writerConfiguration.getTargetBaseDirectory();
	}
}
