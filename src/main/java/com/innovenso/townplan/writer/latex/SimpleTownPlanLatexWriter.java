package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class SimpleTownPlanLatexWriter extends AbstractTownPlanLatexWriter {
	private final String templateName;
	private final String outputFilename;
	private final String fileSuffix;

	public SimpleTownPlanLatexWriter(@NonNull final LatexWriterConfiguration configuration, @NonNull String folderName,
			@NonNull String templateName, @NonNull final String outputFileName, @NonNull final String fileSuffix) {
		super(configuration, folderName);
		this.outputFilename = outputFileName;
		this.templateName = templateName;
		this.fileSuffix = fileSuffix;
	}

	@Override
	public String getLatexTemplateName() {
		return templateName;
	}

	@Override
	protected String getOutputFileName(@NonNull TownPlan townPlan) {
		return outputFilename;
	}

	@Override
	public String getFileSuffix() {
		return fileSuffix;
	}
}
