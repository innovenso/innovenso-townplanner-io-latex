package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.writer.pipeline.ClassPathResourceFileProvider;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Log4j2
public class TownPlanLatexBuildFileWriter implements TownPlanLatexFileWriter {
	private final ClassPathResourceFileProvider fileProvider;
	private final File targetBaseDirectory;

	public TownPlanLatexBuildFileWriter(@NonNull final LatexWriterConfiguration configuration) {
		this.targetBaseDirectory = configuration.getTargetBaseDirectory();
		this.fileProvider = new ClassPathResourceFileProvider();
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, List<File> renderedFiles) {
		List.of("build.sh").stream().map(this::write).forEach(renderedFiles::add);
	}

	private File write(@NonNull final String fileName) {
		final Path outputPath = Paths.get(targetBaseDirectory.getAbsolutePath(), fileName);
		final File directory = outputPath.getParent().toFile();
		directory.mkdirs();
		final File outputFile = outputPath.toFile();
		fileProvider.writeFile(outputFile, "/latex/" + fileName);
		log.debug("writing LaTeX build file {}", outputFile.getAbsolutePath());
		return outputFile;
	}
}
