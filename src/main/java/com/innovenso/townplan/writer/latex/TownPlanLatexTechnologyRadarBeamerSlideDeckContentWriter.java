package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.writer.model.TechnologyRadarPresentationModel;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TownPlanLatexTechnologyRadarBeamerSlideDeckContentWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexTechnologyRadarBeamerSlideDeckContentWriter(
			@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/decks/techradar");
	}

	@Override
	public String getLatexTemplateName() {
		return "techradar-beamer-content";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "content";
	}

	@Override
	protected Object getModel(@NonNull TownPlan townPlan) {
		return new TechnologyRadarPresentationModel(townPlan);
	}
}
