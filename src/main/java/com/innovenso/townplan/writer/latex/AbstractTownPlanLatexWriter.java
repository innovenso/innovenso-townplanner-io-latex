package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.TownPlanWriterModel;
import com.innovenso.townplan.writer.pipeline.freemarker.FreemarkerRenderer;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Log4j2
public abstract class AbstractTownPlanLatexWriter implements TownPlanLatexFileWriter {
	protected final FreemarkerRenderer freemarkerRenderer;
	protected final AssetRepository assetRepository;
	protected final String folderName;
	protected final File targetBaseDirectory;
	protected final LatexWriterConfiguration writerConfiguration;

	public AbstractTownPlanLatexWriter(@NonNull final LatexWriterConfiguration configuration,
			@NonNull final String folderName) {
		this.writerConfiguration = configuration;
		this.freemarkerRenderer = new FreemarkerRenderer("latex");
		this.assetRepository = configuration.getRepository();
		this.folderName = folderName;
		this.targetBaseDirectory = Paths.get(configuration.getTargetBaseDirectory().getAbsolutePath(), folderName)
				.toFile();
		if (!this.targetBaseDirectory.exists())
			this.targetBaseDirectory.mkdirs();
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		final Map<String, Object> model = Map.of("model", getModel(townPlan), "theme", writerConfiguration.getTheme(),
				"institution", writerConfiguration.getInstitution());
		final Optional<File> latexOutputFile = freemarkerRenderer.write(model, getLatexTemplateName() + ".ftl");
		latexOutputFile.ifPresent(render -> {
			final File resultingFile = new File(targetBaseDirectory, getOutputFileName(townPlan) + getFileSuffix());
			try {
				FileUtils.copyFile(render, resultingFile);
				renderedFiles.add(resultingFile);
			} catch (IOException e) {
				log.error(e);
			}
		});
	}

	protected Object getModel(final @NonNull TownPlan townPlan) {
		return new TownPlanWriterModel(townPlan);
	}

	protected abstract String getLatexTemplateName();

	protected abstract String getOutputFileName(@NonNull final TownPlan townPlan);

	public String getOutputPath(@NonNull final TownPlan townPlan) {
		return folderName + "/" + getOutputFileName(townPlan) + ".tex";
	}

	public String getFileSuffix() {
		return ".tex";
	}
}
