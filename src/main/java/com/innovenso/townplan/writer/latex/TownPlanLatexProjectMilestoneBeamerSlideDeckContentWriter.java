package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.writer.model.ItProjectMileStonePresentationModel;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Log4j2
public class TownPlanLatexProjectMilestoneBeamerSlideDeckContentWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexProjectMilestoneBeamerSlideDeckContentWriter(
			@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/decks");
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, @NonNull final List<File> renderedFiles) {
		townPlan.getElements(ItProjectMilestone.class).forEach(milestone -> {
			final Map<String, Object> model = Map.of("model",
					new ItProjectMileStonePresentationModel(townPlan, milestone), "theme",
					writerConfiguration.getTheme(), "institution", writerConfiguration.getInstitution());

			final File milestoneDirectory = new File(targetBaseDirectory, milestone.getKey());
			milestoneDirectory.mkdirs();
			Optional<File> latexOutputFile = freemarkerRenderer.write(model, getLatexTemplateName() + ".ftl");
			latexOutputFile.ifPresent(render -> {
				final File resultingFile = new File(milestoneDirectory, "content.tex");
				try {
					FileUtils.copyFile(render, resultingFile);
					renderedFiles.add(resultingFile);
				} catch (IOException e) {
					log.error(e);
				}
			});
		});
	}

	@Override
	public String getLatexTemplateName() {
		return "milestone-beamer-content";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "milestones";
	}
}
