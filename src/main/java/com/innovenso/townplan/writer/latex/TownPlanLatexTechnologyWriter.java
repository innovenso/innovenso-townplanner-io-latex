package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexTechnologyWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexTechnologyWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "technologies";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "technologies";
	}
}
