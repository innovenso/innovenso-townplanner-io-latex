package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.plantuml.AbstractTownPlanPlantUMLWriter;
import lombok.NonNull;

import java.io.File;

public class TownPlanLatexDiagramWriter extends AbstractTownPlanPlantUMLWriter implements TownPlanLatexFileWriter {
	public TownPlanLatexDiagramWriter(@NonNull final String targetBasePath, @NonNull AssetRepository assetRepository) {
		super(new File(targetBasePath, "latex"), assetRepository, "images", ContentOutputType.LATEX);
	}
}
