package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexFlowWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexFlowWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "flows";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "flows";
	}
}
