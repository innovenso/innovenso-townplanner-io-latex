package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.repository.AssetRepository;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.io.File;

@Value
@Builder
public class LatexWriterConfiguration {
	@NonNull
	String theme;
	@NonNull
	String institution;
	@NonNull
	AssetRepository repository;
	@NonNull
	File targetBaseDirectory;
}
