package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexBusinessActorWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexBusinessActorWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "actors";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "actors";
	}
}
