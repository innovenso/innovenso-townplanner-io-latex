package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.writer.pipeline.ClassPathResourceFileProvider;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@Log4j2
public class TownPlanLatexProjectMilestoneBeamerSlideDeckScaffoldingWriter implements TownPlanLatexFileWriter {
	private final ClassPathResourceFileProvider fileProvider;
	private final File targetBaseDirectory;

	public TownPlanLatexProjectMilestoneBeamerSlideDeckScaffoldingWriter(
			@NonNull final LatexWriterConfiguration configuration) {
		this.targetBaseDirectory = new File(configuration.getTargetBaseDirectory(), "src/decks");
		this.fileProvider = new ClassPathResourceFileProvider();
	}

	@Override
	public void write(@NonNull final TownPlan townPlan, List<File> renderedFiles) {
		townPlan.getElements(ItProjectMilestone.class).forEach(milestone -> {
			Stream.of("presentation.tex", "handouts.tex", "handouts_with_notes.tex", "actions.tex",
					"context_diagram.tex", "dependencies.tex", "financial_impact.tex", "objectief.tex", "solution.tex",
					"image_tester.tex").map(filename -> write(milestone, filename)).forEach(renderedFiles::add);
		});
	}

	private File write(@NonNull final ItProjectMilestone milestone, @NonNull final String fileName) {
		final Path outputPath = Paths.get(targetBaseDirectory.getAbsolutePath(), milestone.getKey(), fileName);
		final File directory = outputPath.getParent().toFile();
		directory.mkdirs();
		final File outputFile = outputPath.toFile();
		fileProvider.writeFile(outputFile, "/latex/" + fileName);
		log.debug("writing LaTeX file {}", outputFile.getAbsolutePath());
		return outputFile;
	}
}
