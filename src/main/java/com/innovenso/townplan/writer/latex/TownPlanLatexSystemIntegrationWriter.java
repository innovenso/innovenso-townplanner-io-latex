package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexSystemIntegrationWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexSystemIntegrationWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "integrations";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "integrations";
	}
}
