package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexArchitectureBuildingBlockWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexArchitectureBuildingBlockWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "building-blocks";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "building-blocks";
	}
}
