package com.innovenso.townplan.writer.latex;

import com.innovenso.townplan.api.TownPlan;
import lombok.NonNull;

public class TownPlanLatexDecisionWriter extends AbstractTownPlanLatexWriter {
	public TownPlanLatexDecisionWriter(@NonNull final LatexWriterConfiguration configuration) {
		super(configuration, "src/townplan");
	}

	@Override
	public String getLatexTemplateName() {
		return "decisions";
	}

	@Override
	public String getOutputFileName(@NonNull TownPlan townPlan) {
		return "decisions";
	}
}
