package com.innovenso.townplan.writer;

import com.innovenso.townplan.TownPlanIO;
import com.innovenso.townplan.reader.TownPlanReader;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.latex.TownPlanLatexWriter;
import lombok.NonNull;

import java.util.Optional;

public record TownPlanLatexIO(@NonNull String targetBasePath, @NonNull String theme, @NonNull String institution,
		@NonNull AssetRepository assetRepository) implements TownPlanIO {
	@Override
	public Optional<TownPlanReader> getReader() {
		return Optional.empty();
	}

	@Override
	public Optional<TownPlanWriter> getWriter() {
		return Optional.of(new TownPlanLatexWriter(targetBasePath, theme, institution, assetRepository));
	}

	@Override
	public String getKey() {
		return "latex";
	}

	@Override
	public String getTitle() {
		return "LaTeX";
	}

	@Override
	public String getDescription() {
		return "Document preparation system used to generate slide decks and documentation in PDF format";
	}
}
