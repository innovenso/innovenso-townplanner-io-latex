package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.aspects.*;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.ItProjectMilestone;
import com.innovenso.townplan.api.value.it.ItSystemIntegration;
import com.innovenso.townplan.api.value.it.decision.Decision;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class ItProjectMileStonePresentationModel {
	private final TownPlan townPlan;
	private final ItProjectMilestone milestone;

	public ItProjectMileStonePresentationModel(@NonNull final TownPlan townPlan,
			@NonNull final ItProjectMilestone milestone) {
		this.townPlan = townPlan;
		this.milestone = milestone;
	}

	public String getTitle() {
		return milestone.getTitle();
	}

	public String getDescription() {
		return milestone.getDescription();
	}

	public String getCamelCasedKey() {
		return milestone.getCamelCasedKey();
	}

	public List<ItSystemIntegration> getAddedItSystemIntegrations() {
		return milestone.getAddedItSystemIntegrations().stream()
				.sorted(Comparator.comparing(ItSystemIntegration::getTitle)).collect(Collectors.toList());
	}

	public List<ItSystemIntegration> getRemovedItSystemIntegrations() {
		return milestone.getRemovedItSystemIntegrations().stream()
				.sorted(Comparator.comparing(ItSystemIntegration::getTitle)).collect(Collectors.toList());
	}

	public List<ItSystemIntegration> getChangedItSystemIntegrations() {
		return milestone.getChangedItSystemIntegrations().stream()
				.sorted(Comparator.comparing(ItSystemIntegration::getTitle)).collect(Collectors.toList());
	}

	public List<SecurityConcern> getSecurityConcerns() {
		return milestone.getSecurityConcerns().stream().sorted(Comparator.comparing(SecurityConcern::getSortKey))
				.collect(Collectors.toList());
	}

	public List<SecurityImpact> getSecurityImpacts() {
		return milestone.getSecurityImpacts().stream().sorted(Comparator.comparing(SecurityImpact::getSortKey))
				.collect(Collectors.toList());
	}

	public List<BusinessActor> getNemawashiDone() {
		return getNemawashiPeople(RelationshipType.HAS_BEEN_CONSULTED, RelationshipType.HAS_BEEN_INFORMED);
	}

	public List<BusinessActor> getNemawashiTodo() {
		return getNemawashiPeople(RelationshipType.TO_BE_CONSULTED, RelationshipType.TO_BE_INFORMED);
	}

	private List<BusinessActor> getNemawashiPeople(RelationshipType... relationshipTypes) {
		return milestone.getIncomingRelationships().stream()
				.filter(relationship -> Set.of(relationshipTypes).contains(relationship.getRelationshipType()))
				.map(Relationship::getSource).filter(BusinessActor.class::isInstance).map(BusinessActor.class::cast)
				.sorted(Comparator.comparing(BusinessActor::getTitle)).collect(Collectors.toList());
	}

	public List<Constraint> getConstraints() {
		return milestone.getConstraints().stream().sorted(Comparator.comparing(Constraint::getSortKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getStrengths() {
		return milestone.getSwots(SWOTType.STRENGTH).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getWeaknesses() {
		return milestone.getSwots(SWOTType.WEAKNESS).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getOpportunities() {
		return milestone.getSwots(SWOTType.OPPORTUNITY).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getThreats() {
		return milestone.getSwots(SWOTType.THREAT).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<FunctionalRequirement> getFunctionalRequirements() {
		return milestone.getFunctionalRequirements().stream()
				.sorted(Comparator.comparing(FunctionalRequirement::getSortKey)).collect(Collectors.toList());
	}

	public List<QualityAttributeRequirement> getQars() {
		return milestone.getQualityAttributes().stream()
				.sorted(Comparator.comparing(QualityAttributeRequirement::getSortKey)).collect(Collectors.toList());
	}

	public List<Decision> getDecisions() {
		return milestone.getDirectDependencies(Decision.class).stream().map(Decision.class::cast)
				.sorted(Comparator.comparing(Decision::getSortKey)).collect(Collectors.toList());
	}
}
