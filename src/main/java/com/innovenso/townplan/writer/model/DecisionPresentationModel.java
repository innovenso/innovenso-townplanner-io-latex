package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.api.value.RelationshipType;
import com.innovenso.townplan.api.value.aspects.*;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.it.decision.Decision;
import com.innovenso.townplan.api.value.it.decision.DecisionContext;
import com.innovenso.townplan.api.value.it.decision.DecisionContextType;
import com.innovenso.townplan.api.value.it.decision.DecisionOption;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.extern.log4j.Log4j2;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class DecisionPresentationModel {
	private final TownPlan townPlan;
	private final Decision decision;

	public DecisionPresentationModel(@NonNull final TownPlan townPlan, @NonNull final Decision decision) {
		this.townPlan = townPlan;
		this.decision = decision;
	}

	public String getTitle() {
		return decision.getTitle();
	}

	public String getDescription() {
		return decision.getDescription();
	}

	public boolean hasLongList() {
		return decision.getOptions().size() > 3;
	}

	public List<String> getFurtherDescriptions() {
		return decision.getDocumentations().stream().filter(doc -> doc.getType().equals(DocumentationType.DESCRIPTION))
				.sorted(Comparator.comparing(doc -> doc.getSortKey() == null ? doc.getKey() : doc.getSortKey()))
				.map(Documentation::getDescription).collect(Collectors.toList());
	}

	public List<String> getUrls() {
		return decision.getDocumentations().stream().filter(doc -> doc.getType().equals(DocumentationType.URL))
				.sorted(Comparator.comparing(doc -> doc.getSortKey() == null ? doc.getKey() : doc.getSortKey()))
				.map(Documentation::getDescription).collect(Collectors.toList());
	}

	public String getOutcome() {
		return decision.getOutcome();
	}

	public String getCamelCasedKey() {
		return decision.getCamelCasedKey();
	}

	public List<BusinessActor> getNemawashiApprovers() {
		return getNemawashiPeople(RelationshipType.ACCOUNTABLE);
	}

	public List<BusinessActor> getNemawashiOwners() {
		return getNemawashiPeople(RelationshipType.RESPONSIBLE);
	}

	public List<BusinessActor> getNemawashiStakeholders() {
		return getNemawashiPeople(RelationshipType.HAS_BEEN_CONSULTED, RelationshipType.HAS_BEEN_INFORMED);
	}

	public boolean hasNemawashi() {
		return !getNemawashiPeople(RelationshipType.HAS_BEEN_CONSULTED, RelationshipType.HAS_BEEN_INFORMED,
				RelationshipType.ACCOUNTABLE, RelationshipType.RESPONSIBLE).isEmpty();
	}

	public boolean hasContext() {
		return !(getCurrentStates().isEmpty() && getGoals().isEmpty() && getAssumptions().isEmpty()
				&& getDescription().isEmpty());
	}

	public boolean hasOptions() {
		return !getOptions().isEmpty();
	}

	public List<DecisionContext> getCurrentStates() {
		return getContexts(DecisionContextType.CURRENT_STATE);
	}

	public List<DecisionContext> getGoals() {
		return getContexts(DecisionContextType.GOAL);
	}

	public List<DecisionContext> getAssumptions() {
		return getContexts(DecisionContextType.ASSUMPTION);
	}

	private List<DecisionContext> getContexts(DecisionContextType decisionContextType) {
		return decision.getContexts().stream()
				.filter(decisionContext -> decisionContext.getContextType().equals(decisionContextType))
				.sorted(Comparator.comparing(Concept::getSortKey)).collect(Collectors.toList());
	}

	public List<DecisionOption> getOptions() {
		return decision.getOptions().stream().sorted(Comparator.comparing(DecisionOption::getSortKey))
				.collect(Collectors.toList());
	}

	public String getOptionKey(DecisionOption option) {
		return option.getTitle().replaceAll("[^a-zA-Z0-9]", "").toLowerCase(Locale.ROOT);
	}

	public List<CostRow> getCosts(DecisionOption option, Integer fiscalYear) {
		return option.getCosts().stream().filter(cost -> cost.getFiscalYear().equals(fiscalYear))
				.map(cost -> CostRow.builder().title(cost.getTitle()).category(cost.getCategory())
						.remarks(cost.getDescription())
						.capex(cost.getCostType().equals(CostType.CAPEX)
								? "\\texteuro " + String.valueOf(cost.getTotalCost())
								: "")
						.opex(cost.getCostType().equals(CostType.OPEX)
								? "\\texteuro " + String.valueOf(cost.getTotalCost())
								: "")
						.build())
				.collect(Collectors.toList());
	}

	public CostRow getTotalCost(DecisionOption option, Integer fiscalYear) {
		Double capex = getTotalCost(option, fiscalYear, CostType.CAPEX);
		Double opex = getTotalCost(option, fiscalYear, CostType.OPEX);
		return CostRow.builder().title("total").category("").remarks("").capex(capex > 0 ? "\\texteuro " + capex : "")
				.opex(opex > 0 ? "\\texteuro " + opex : "").build();
	}

	public List<Cost> getCosts(DecisionOption option, @NonNull final Integer fiscalYear,
			@NonNull final CostType costType) {
		return option.getCosts().stream().filter(cost -> fiscalYear.equals(cost.getFiscalYear()))
				.filter(cost -> costType.equals(cost.getCostType())).sorted(Comparator.comparing(Cost::getCategory))
				.collect(Collectors.toList());
	}

	public Double getTotalCost(DecisionOption option, @NonNull final Integer fiscalYear,
			@NonNull final CostType costType) {
		return getCosts(option, fiscalYear, costType).stream().mapToDouble(Cost::getTotalCost).sum();
	}

	public List<Constraint> getConstraints() {
		return decision.getConstraints().stream().sorted(Comparator.comparing(Constraint::getSortKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getStrengths(DecisionOption option) {
		return option.getSwots(SWOTType.STRENGTH).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getWeaknesses(DecisionOption option) {
		return option.getSwots(SWOTType.WEAKNESS).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getOpportunities(DecisionOption option) {
		return option.getSwots(SWOTType.OPPORTUNITY).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<SWOT> getThreats(DecisionOption option) {
		return option.getSwots(SWOTType.THREAT).stream().sorted(Comparator.comparing(SWOT::getKey))
				.collect(Collectors.toList());
	}

	public List<FunctionalRequirement> getFunctionalRequirements() {
		return decision.getFunctionalRequirements().stream()
				.sorted(Comparator.comparing(FunctionalRequirement::getSortKey)).collect(Collectors.toList());
	}

	public List<QualityAttributeRequirement> getQars() {
		return decision.getQualityAttributes().stream()
				.sorted(Comparator.comparing(QualityAttributeRequirement::getSortKey)).collect(Collectors.toList());
	}

	public String getExceedColumn(DecisionOption option, FunctionalRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.EXCEEDS_EXPECTATIONS);
	}

	public String getMeetColumn(DecisionOption option, FunctionalRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.MEETS_EXPECTATIONS);
	}

	public String getAlmostColumn(DecisionOption option, FunctionalRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.ALMOST_MEETS_EXPECTATIONS);
	}

	public String getLackColumn(DecisionOption option, FunctionalRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.DOES_NOT_MEET_EXPECTATIONS);
	}

	public String getExceedColumn(DecisionOption option, QualityAttributeRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.EXCEEDS_EXPECTATIONS);
	}

	public String getMeetColumn(DecisionOption option, QualityAttributeRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.MEETS_EXPECTATIONS);
	}

	public String getAlmostColumn(DecisionOption option, QualityAttributeRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.ALMOST_MEETS_EXPECTATIONS);
	}

	public String getLackColumn(DecisionOption option, QualityAttributeRequirement requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.DOES_NOT_MEET_EXPECTATIONS);
	}

	public String getExceedColumn(DecisionOption option, Constraint requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.EXCEEDS_EXPECTATIONS);
	}

	public String getMeetColumn(DecisionOption option, Constraint requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.MEETS_EXPECTATIONS);
	}

	public String getAlmostColumn(DecisionOption option, Constraint requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.ALMOST_MEETS_EXPECTATIONS);
	}

	public String getLackColumn(DecisionOption option, Constraint requirement) {
		return getScoreColumn(option, requirement, ScoreWeight.DOES_NOT_MEET_EXPECTATIONS);
	}

	private String getScoreColumn(DecisionOption option, FunctionalRequirement requirement, ScoreWeight scoreWeight) {
		return getScoreColumn(option.getFunctionalRequirementScore(requirement.getKey()), scoreWeight);
	}

	private String getScoreColumn(DecisionOption option, QualityAttributeRequirement requirement,
			ScoreWeight scoreWeight) {
		return getScoreColumn(option.getQualityAttributeScore(requirement.getKey()), scoreWeight);
	}

	private String getScoreColumn(DecisionOption option, Constraint requirement, ScoreWeight scoreWeight) {
		return getScoreColumn(option.getQualityAttributeScore(requirement.getKey()), scoreWeight);
	}

	private String getScoreColumn(ScoreWeight weight, ScoreWeight expectedWeight) {
		if (weight.equals(expectedWeight))
			return "\\faCheck";
		else
			return "";
	}

	private List<BusinessActor> getNemawashiPeople(RelationshipType... relationshipTypes) {
		return decision.getIncomingRelationships().stream()
				.filter(relationship -> Set.of(relationshipTypes).contains(relationship.getRelationshipType()))
				.map(Relationship::getSource).filter(BusinessActor.class::isInstance).map(BusinessActor.class::cast)
				.sorted(Comparator.comparing(BusinessActor::getTitle)).collect(Collectors.toList());
	}

	public String getKey() {
		return decision.getCamelCasedKey();
	}

	@Value
	@Builder
	public static final class CostRow {
		String title;
		String category;
		String remarks;
		String capex;
		String opex;
	}
}
