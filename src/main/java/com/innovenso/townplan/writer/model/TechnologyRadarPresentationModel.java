package com.innovenso.townplan.writer.model;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.enums.TechnologyType;
import com.innovenso.townplan.api.value.it.Technology;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
public class TechnologyRadarPresentationModel {
	private final TownPlan townPlan;

	public TechnologyRadarPresentationModel(@NonNull final TownPlan townPlan) {
		this.townPlan = townPlan;
	}

	public List<Technology> getLanguagesAndFrameworks() {
		return getTechnologyType(TechnologyType.LANGUAGE_FRAMEWORK);
	}

	public List<Technology> getTools() {
		return getTechnologyType(TechnologyType.TOOL);
	}

	public List<Technology> getTechniques() {
		return getTechnologyType(TechnologyType.TECHNIQUE);
	}

	public List<Technology> getPlatforms() {
		return getTechnologyType(TechnologyType.PLATFORM);
	}

	private List<Technology> getTechnologyType(@NonNull final TechnologyType technologyType) {
		return townPlan.getElements(Technology.class).stream()
				.filter(technology -> Objects.equals(technologyType, technology.getTechnologyType()))
				.sorted(Comparator.comparing(Technology::getTitle)).collect(Collectors.toList());
	}
}
