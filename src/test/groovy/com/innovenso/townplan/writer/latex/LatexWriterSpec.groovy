package com.innovenso.townplan.writer.latex

import com.google.common.io.Files
import com.innovenso.townplan.api.value.it.decision.DecisionContextType
import com.innovenso.townplan.domain.TownPlanImpl
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.AssetRepository
import com.innovenso.townplan.repository.FileSystemAssetRepository
import spock.lang.Specification
import spock.lang.TempDir

class LatexWriterSpec extends Specification {
	File writePath
	File assetRepositoryPath
	AssetRepository assetRepository
	TownPlanImpl townPlan
	SampleFactory samples
	List<File> renderedFiles

	TownPlanLatexWriter serviceUnderTest

	def setup() {
		writePath = Files.createTempDir()
		assetRepositoryPath = Files.createTempDir()
		assetRepository = new FileSystemAssetRepository(assetRepositoryPath.getAbsolutePath(), assetRepositoryPath.toURI().toURL().toString())
		townPlan = new TownPlanImpl()
		samples = new SampleFactory(townPlan)
		renderedFiles = []
		serviceUnderTest = new TownPlanLatexWriter(writePath.getAbsolutePath(), "innovenso", "Innovenso BV", assetRepository)
	}

	def "writing the tech radar does not result in an exception"() {
		given:
		def enterprise = samples.enterprise()
		20.times {
			samples.technology()
		}
		when:
		serviceUnderTest.write(townPlan, renderedFiles)
		then:
		!renderedFiles.isEmpty()
	}

	def "writing decisions does not result in an exception"() {
		given:
		def enterprise = samples.enterprise()
		def decision = samples.decision(enterprise)
		3.times {
			def actor = samples.actor(enterprise)
			samples.responsible(actor, decision)
		}
		2.times {
			def actor = samples.actor(enterprise)
			samples.accountable(actor, decision)
		}

		6.times {
			def actor = samples.actor(enterprise)
			samples.consulted(actor, decision)
		}

		2.times {
			def actor = samples.actor(enterprise)
			samples.informed(actor, decision)
		}

		15.times {
			samples.decisionContext(decision, samples.randomEnum(DecisionContextType.class))
		}
		12.times {
			def option = samples.option(decision)
		}

		List<File> renderedFiles = []
		when:
		serviceUnderTest.write(townPlan, renderedFiles)
		then:
		!renderedFiles.isEmpty()
		renderedFiles.each {
			println it.absolutePath
			if (it.absolutePath.endsWith('.longlist.tikz')) println it.text
		}
	}
}
